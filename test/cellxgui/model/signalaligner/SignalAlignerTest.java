/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cellxgui.model.signalaligner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author chris
 */
public class SignalAlignerTest {
    
    public SignalAlignerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testAddSignal() {
        
        SignalAligner a = new SignalAligner();           
        for(int i=0; i<200; ++i){
            TestSignal ts = new TestSignal(50, 20);
            a.addSignal(ts);                      
        }      
        System.out.println("Average coverage: " + a.getAverageSignalCoverage());   
        System.out.println(a.toOctave());   
    }
    
    @Test
    public void testAddSignal2() {
        
        SignalAligner a = new SignalAligner();           
        for(int i=0; i<200; ++i){
            TestSignal2 ts = new TestSignal2(50, 20);
            a.addSignal(ts);                      
        }      
        System.out.println("Average coverage: " + a.getAverageSignalCoverage());   
        System.out.println(a.toOctave());   
        System.out.println(a.toTrimmedOctave()); 
    }

    @Test
    public void testFindBestAlnPos() {
        
        double[] a = {0,0,1,0,1,1,0};
     
        int[] pos = SignalAligner.findBestAlnPos(a, a);
        assertArrayEquals(new int[] {0,0}, pos);
                
        a = new double[] {1,0,1};
        double[] b = {0,0,0,1,0,1,0,0};
        pos = SignalAligner.findBestAlnPos(a, b);
        assertArrayEquals(new int[] {0,3}, pos);
       
        a = new double[] {1,0,1,0,0,0,0,0,0,0,0,0,0};
        pos = SignalAligner.findBestAlnPos(a, b);
        assertArrayEquals(new int[] {0,3}, pos);
               
        a = new double[] {1,0,1};
        b = new double[] {0,0,0,0,0,1,0,1};
        pos = SignalAligner.findBestAlnPos(a, b);
        assertArrayEquals(new int[] {0,5}, pos);
        pos = SignalAligner.findBestAlnPos(b,a);
        assertArrayEquals(new int[] {5,0}, pos);
        
        a = new double[] {1,0,1};
        b = new double[] {1,0,1,0,0,0,0,1};
        pos = SignalAligner.findBestAlnPos(a, b);
        assertArrayEquals(new int[] {0,0}, pos);
        pos = SignalAligner.findBestAlnPos(b, a);
        assertArrayEquals(new int[] {0,0}, pos);
        
        a = new double[] {0,1,1};
        b = new double[] {1,1,0,0,0,0,0,1,0,1};
        pos = SignalAligner.findBestAlnPos(a, b);
        assertArrayEquals(new int[] {1,0}, pos);
        pos = SignalAligner.findBestAlnPos(b, a);
        assertArrayEquals(new int[] {0,1}, pos);
        
         
        a = new double[] {1,1,0};
        b = new double[] {1,0,0,0,0,0,0,0,1,1};
        pos = SignalAligner.findBestAlnPos(a, b);
        assertArrayEquals(new int[] {0,8}, pos);       
        pos = SignalAligner.findBestAlnPos( b,a);
        assertArrayEquals(new int[] {8,0}, pos);
    }
}
