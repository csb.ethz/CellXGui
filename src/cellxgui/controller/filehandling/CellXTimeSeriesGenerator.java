/*
 * CellXTimeSeriesGenerator.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller.filehandling;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXTimeSeriesGenerator {

    public final static String NO_POSITION = "";
    private final Pattern outOfFocusPattern;
    private final List<Pattern> fluoPatterns;
    private final Pattern frameIdxPattern;
    private final Pattern flatFieldPattern;
    private final File srcDir;
    private final File dstDir;
    private final boolean trackingEnabled;

    public CellXTimeSeriesGenerator(File srcDir, File dstDir, Pattern outOfFocusPattern, List<Pattern> fluoPatterns, Pattern frameIdxPattern, Pattern flatFieldPattern, boolean doTracking) {
        this.outOfFocusPattern = outOfFocusPattern;
        this.fluoPatterns = fluoPatterns;
        this.frameIdxPattern = frameIdxPattern;
        this.flatFieldPattern = flatFieldPattern;
        this.srcDir = srcDir;
        this.dstDir = dstDir;
        this.trackingEnabled = doTracking;
    }

    public List<CellXTimeSeries> getTimeSeries() throws FileSetCollisionException, NoOutOfFocusImagesFoundException {

        /*
        System.out.println("OOF PATTERN:  " +outOfFocusPattern);
        System.out.println("FRAME IDX PATTERN:  " +frameIdxPattern);
        System.out.println("SRC DIR: " + srcDir);
         */

        final Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets = collectFileSetsPerPosition();

        if (position2fileSets.isEmpty()) {
            throw new NoOutOfFocusImagesFoundException(outOfFocusPattern, frameIdxPattern);
        }

        collectFluoFilesPerPosition(position2fileSets);

        final Map<String, Map<String, File>> position2fluoType2flatField = collectFlatFieldImages(position2fileSets);

        final List<CellXTimeSeries> timeSeriesList = createTimeSeries(position2fileSets, position2fluoType2flatField);

        Collections.sort(timeSeriesList, new Comparator<CellXTimeSeries>() {

            @Override
            public int compare(CellXTimeSeries o1, CellXTimeSeries o2) {
                return o1.size() - o2.size();
            }
        });

        for (int i = 0; i < timeSeriesList.size(); ++i) {
            final CellXTimeSeries ts = timeSeriesList.get(i);
            ts.setId(i + 1);
            ts.setDoTracking(trackingEnabled);
            ts.setBaseResultDirectory(dstDir);
            ts.generateName();
        }

        return timeSeriesList;

    }

    /*
     * Creates a map that associates the name of every subfolder in srcDir to a set with the contained files
     * The images directly in srcDir are mapped to the empty string
     */
    private Map<String, Map<CellXFileSet, CellXFileSet>> collectFileSetsPerPosition() throws FileSetCollisionException {
        final Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets = new HashMap<String, Map<CellXFileSet, CellXFileSet>>();
        final File[] dirContents = srcDir.listFiles();
        if (dirContents == null) {
            return position2fileSets;
        }
        for (File file : dirContents) {
            if (file.isDirectory()) {
                final File[] subdirContents = file.listFiles();
                if (subdirContents != null) {
                    final String position = file.getName();
                    for (File subFile : subdirContents) {
                        registerForPosition(position, subFile, position2fileSets);
                    }
                }
            } else {
                registerForPosition(NO_POSITION, file, position2fileSets);
            }
        }
        return position2fileSets;
    }

    private CellXFileSet getFileSetFromOof(File f) {
        final String fname = f.getName();
        if (outOfFocusPattern.matcher(fname).matches()) {
            final Matcher m = frameIdxPattern.matcher(fname);
            if (m.matches()) {
                try {
                    final Integer frameIdx = Integer.parseInt(m.group(1));
                    return new CellXFileSet(frameIdx, f);
                } catch (NumberFormatException e) {
                    //do nothing, skip the file
                }
            }
        }
        return null;
    }

    private void registerForPosition(String position, File file, Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets) throws FileSetCollisionException {
        final CellXFileSet fs = getFileSetFromOof(file);
        if (fs == null) {
            return;
        }
        // throw collision error if there is a file set with this frame number in this position already
        if (position2fileSets.containsKey(position)) {
            if (!position2fileSets.get(position).containsKey(fs)) {
                position2fileSets.get(position).put(fs, fs);
            } else {
                throw new FileSetCollisionException(position2fileSets.get(position).get(fs), fs);
            }
        } else {
            final Map<CellXFileSet, CellXFileSet> map = new HashMap<CellXFileSet, CellXFileSet>();
            map.put(fs, fs);
            position2fileSets.put(position, map);
        }
    }

    private void collectFluoFilesPerPosition(Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets) {
        final File[] dirContents = srcDir.listFiles();
        if (dirContents == null) {
            return;
        }
        for (File file : dirContents) {
            if (file.isDirectory()) {
                final File[] subdirContents = file.listFiles();
                if (subdirContents != null) {
                    final String position = file.getName();
                    for (File subFile : subdirContents) {
                        if (position2fileSets.containsKey(position)) {
                            registerFluorescenceImageForPosition(position, subFile, position2fileSets);
                        }
                    }
                }
            } else {
                if (position2fileSets.containsKey(NO_POSITION)) {
                    registerFluorescenceImageForPosition(NO_POSITION, file, position2fileSets);
                }
            }
        }
    }

    private void registerFluorescenceImageForPosition(String position, File file, Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets) {
        final String fName = file.getName();
        for (Pattern fluoPat : fluoPatterns) {
            Matcher m = fluoPat.matcher(fName);
            if (m.matches()) {
                String type = m.group(1).trim();
                m = frameIdxPattern.matcher(fName);
                if (m.matches()) {
                    try {
                        final Integer frameIdx = Integer.parseInt(m.group(1));
                        final CellXFileSet key = new CellXFileSet(frameIdx, null);
                        if (position2fileSets.get(position).containsKey(key)) {
                            position2fileSets.get(position).get(key).addFluoImage(new CellXFluoFile(file, type));
                        }
                    } catch (NumberFormatException e) {
                        // skip this file
                    }
                }
            }
        }
    }

    private Map<String, Map<String, File>> collectFlatFieldImages(Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets) {
        final Map<String, Map<String, File>> ret = new HashMap<String, Map<String, File>>();
        final File[] dirContents = srcDir.listFiles();
        if (dirContents == null || flatFieldPattern == null) {
            return ret;
        }
        for (File file : dirContents) {
            if (file.isDirectory()) {
                final File[] subdirContents = file.listFiles();
                if (subdirContents != null) {
                    final String position = file.getName();
                    for (File subFile : subdirContents) {
                        if (position2fileSets.containsKey(position)) {
                            registerFlatFieldForPosition(position, subFile, ret);
                        }
                    }
                }
            } else {
                if (position2fileSets.containsKey(NO_POSITION)) {
                    registerFlatFieldForPosition(NO_POSITION, file, ret);
                }
            }
        }
        return ret;
    }

    private void registerFlatFieldForPosition(String position, File file, Map<String, Map<String, File>> ret) {
        final String fName = file.getName();
        final Matcher m = flatFieldPattern.matcher(fName);
        if (m.matches()) {
            final String fluoType = m.group(1).trim();
            if (ret.containsKey(position)) {
                if (!ret.get(position).containsKey(fluoType)) {
                    ret.get(position).put(fluoType, file);
                }
            } else {
                final Map<String, File> tmp = new HashMap<String, File>();
                tmp.put(fluoType, file);
                ret.put(position, tmp);
            }
        }
    }

    private List<CellXTimeSeries> createTimeSeries(Map<String, Map<CellXFileSet, CellXFileSet>> position2fileSets, Map<String, Map<String, File>> position2fluoType2flatField) {

        final Map<CellXTimeSeries, CellXTimeSeries> map = new HashMap<CellXTimeSeries, CellXTimeSeries>();

        for (String position : position2fileSets.keySet()) {

            for (CellXFileSet fs : position2fileSets.get(position).keySet()) {

                fillInFlatFieldImages(position, fs, position2fluoType2flatField);

                final CellXTimeSeries tmp = new CellXTimeSeries(fs.getFluoTypes(), position);

                if (map.containsKey(tmp)) {
                    map.get(tmp).addFileSet(fs);
                } else {
                    tmp.addFileSet(fs);
                    map.put(tmp, tmp);
                }
            }
        }

        for (CellXTimeSeries ts : map.keySet()) {
            ts.sort();
        }

        return new ArrayList<CellXTimeSeries>(map.keySet());
    }

    private void fillInFlatFieldImages(String position, CellXFileSet fs, Map<String, Map<String, File>> position2fluoType2flatField) {
        if (position2fluoType2flatField.containsKey(position)) {
            final Map<String, File> map = position2fluoType2flatField.get(position);
            for (String fluoType : fs.getFluoTypes()) {
                if (map.containsKey(fluoType)) {
                    fs.getFluoFile(fluoType).setFlatFieldImage(map.get(fluoType));
                }
            }
        }
    }
}
