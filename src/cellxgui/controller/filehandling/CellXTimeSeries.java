/*
 * CellXTimeSeries.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller.filehandling;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXTimeSeries {

    private int id = -1;
    private boolean doTracking = true;
    private File baseResultDirectory = null;
    private final String position;
    private final String[] fluoTypes;
    private final List<CellXFileSet> fileSets = new ArrayList<CellXFileSet>();
    private String name = null;

    public CellXTimeSeries(String[] fluoTypes, String position) {
        this.fluoTypes = fluoTypes;
        this.position = position;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CellXTimeSeries) {
            final CellXTimeSeries other = (CellXTimeSeries) obj;
            if (other.position.equals(position)) {
                return Arrays.deepEquals(other.fluoTypes, fluoTypes);
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.position != null ? this.position.hashCode() : 0);
        hash = 67 * hash + Arrays.deepHashCode(this.fluoTypes);
        return hash;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDoTracking(boolean doTracking) {
        this.doTracking = doTracking;
    }

    public void addFileSet(CellXFileSet fs) {
        fileSets.add(fs);
    }

    public void sort() {
        Collections.sort(fileSets);
    }

    public int size() {
        return fileSets.size();
    }

    public void setBaseResultDirectory(File dstDir) {
        this.baseResultDirectory = dstDir;
    }

    public File getResultDirectory() {
        if (baseResultDirectory == null) {
            return null;
        }
        if (position.equals(CellXTimeSeriesGenerator.NO_POSITION)) {
            return baseResultDirectory;
        } else {
            return new File(baseResultDirectory, position);
        }
    }

    @Override
    public String toString() {
        return name == null ? super.toString() : name;
    }

    private boolean[] initFFCounts() {
        final boolean[] ret = new boolean[fluoTypes.length];
        if (!fileSets.isEmpty()) {
            final CellXFileSet fs = fileSets.get(0);
            int i = 0;
            for (String fluoType : fs.getFluoTypes()) {
                ret[i++] = fs.getFluoFile(fluoType).hasFlatFieldImage();
            }

        }
        return ret;
    }

    public void generateName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(String.format("Series %-3d: %4d frame(s) ", id, fileSets.size()));
        if (fluoTypes.length == 0) {
            sb.append(" (no fluorescence images)");
        } else {
            sb.append("(");
            final boolean[] ffCounts = initFFCounts();
            for (int i = 0; i < fluoTypes.length; ++i) {
                sb.append(fluoTypes[i]);
                if (ffCounts[i]) {
                    sb.append("[+FF]");
                }
                if (i < fluoTypes.length - 1) {
                    sb.append(", ");
                }
            }
            sb.append(")");
        }
        this.name = sb.toString();
    }

    public int getId() {
        return id;
    }

    public String getPosition() {
        return position;
    }

    public boolean isTrackingEnabled() {
        return doTracking;
    }

    public String getFluoTypesString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fluoTypes.length; ++i) {
            sb.append(fluoTypes[i]);
            if (i < fluoTypes.length - 1) {
                sb.append("_");
            }
        }
        return sb.toString();
    }

    public Iterable<CellXFileSet> getFileSets() {
        return fileSets;
    }
}
