/*
 * CellXFileSet.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller.filehandling;

import java.io.File;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXFileSet implements Comparable<CellXFileSet> {

    public final static int NO_POSITION = -1;
    private final int frameIdx;
    private final File outOfFocusImg;
    private final SortedSet<CellXFluoFile> fluoFiles = new TreeSet<CellXFluoFile>();

    public CellXFileSet(int frameIdx, File outOfFocusImg) {
        this.frameIdx = frameIdx;
        this.outOfFocusImg = outOfFocusImg;
    }

    CellXFileSet(int frameIdx) {
        this(frameIdx, new File(""));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CellXFileSet) {
            final CellXFileSet fs = (CellXFileSet) obj;
            if (fs.frameIdx == this.frameIdx) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.frameIdx;
        return hash;
    }

    @Override
    public int compareTo(CellXFileSet o) {
        return this.frameIdx - o.frameIdx;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder();

        String title = String.format("Frame: %d", frameIdx);
        sb.append(title).append("\n");

        sb.append("  OOF image: ").append(outOfFocusImg.toString()).append("\n");

        for (CellXFluoFile f : fluoFiles) {
            sb.append("  Fluo image: ").append(f.toString()).append("\n");
        }

        return sb.toString();
    }

    public void addFluoImage(CellXFluoFile f) {
        if (!fluoFiles.add(f)) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Found mutliple fluo files with the same type for the same frame {0}", f);
        }
    }

    public String[] getFluoTypes() {
        final String[] types = new String[fluoFiles.size()];
        int i = 0;
        for (CellXFluoFile f : fluoFiles) {
            types[i++] = f.getType();
        }
        return types;
    }

    public CellXFluoFile getFluoFile(String type) {
        final Iterator<CellXFluoFile> it = fluoFiles.iterator();
        CellXFluoFile ret = null;
        while (it.hasNext()) {
            ret = it.next();
            if (ret.getType().equals(type)) {
                return ret;
            }
        }
        return null;
    }

    public Iterable<CellXFluoFile> getFluoFiles() {
        return fluoFiles;
    }

    public int getFrameIdx() {
        return frameIdx;
    }

    public File getOutOfFocusImage() {
        return outOfFocusImg;
    }
}
