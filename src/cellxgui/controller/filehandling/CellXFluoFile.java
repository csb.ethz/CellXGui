/*
 * CellXFluoFile.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This fluoImage is part of CellXGui
 *
 */
package cellxgui.controller.filehandling;

import java.io.File;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXFluoFile implements Comparable<CellXFluoFile> {

    private final String type;
    private final File fluoImage;
    private File flatFieldFile;

    public CellXFluoFile(File f, String tag) {
        this.fluoImage = f;
        this.type = tag;
    }

    @Override
    public int compareTo(CellXFluoFile o) {
        return this.type.compareTo(o.type);
    }

    @Override
    public String toString() {
        return fluoImage.toString();
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CellXFluoFile) {
            final CellXFluoFile other = (CellXFluoFile) obj;
            return type.equals(other.type);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }

    void setFlatFieldImage(File file) {
        this.flatFieldFile = file;
    }

    public boolean hasFlatFieldImage() {
        return flatFieldFile != null;
    }

    public File getFluoImage() {
        return fluoImage;
    }

    public File getFlatFieldImage() {
        return flatFieldFile;
    }
}
