/*
 * CellXGuiConfiguration.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXGuiConfiguration {

    public static final int version = 2;
    public static final int release = 12;
    public static double percentageThrCoverage = 0.7;
    public static Color selectionColor = Color.blue;
    public final static int CLAHE_BLOCK_RADIUS = 50;
    public final static int CLAHE_BINS = 255;
    public final static float CLAHE_SLOPE = 2.0f;
    public static String execCellXHoughMode = "0";
    public static String execCellXSegmentationMode = "1";
    public static String CellXProjectFilenameExtension = "clx";
    public static int MAX_DIR_REC_DEPTH = 5;
    public static String HoughTransformName = "Seeding";
    public static String SegmentationName = "Segmentation";
    public static String SegmentationFluoImageMenuName = "Intensity mask";
    public static String DEFAULT_CALIBRATION_FILE_NAME = "calibration.xml";
    public static String DEFAULT_FILE_SERIES_FILE_NAME = "fileseries.xml";
    public static int IMAGE_VIEWER_WIDTH = 600;
    public static int IMAGE_VIEWER_HEIGHT = 800;
    public static int MIN_CROP_WIDTH = 50;
    public static String CellXFilesXMLRootNodeName = "CellXFiles";
    public static String CellXTimeSeriesXMLNodeName = "CellXTimeSeries";
    public static String CellXResultDirXMLNodeName = "CellXResultDir";
    public static String CellXTimeSeriesXMLIdAttr = "id";
    public static String CellXTimeSeriesXMLPositionAttr = "position";
    public static String CellXTimeSeriesXMLFluoTypesAttr = "fluotypes";
    public static String CellXTimeSeriesXMLTrackingAttr = "tracking";
    public static String CellXFileSetXMLNodeName = "CellXFileSet";
    public static String CellXFileSetFrameAttrName = "frame";
    public static String CellXOofImageXMLNodeName = "oofImage";
    public static String CellXFluoImageXMLNodeName = "fluoImage";
    public static String CellXFfImageXMLNodeName = "ffImage";
    public static String CellXFluoSetXMLNodeName = "fluoSet";
    public static String CellXFluoTypeAttrName = "type";

    public static String CellXURL = "http://www.csb.ethz.ch/tools/cellx";
    public static String ImageJURL = "http://rsbweb.nih.gov/ij/";
    public static String CLAHEURL = "http://fiji.sc/wiki/index.php/CLAHE";
    public static String JFreeChartURL = "http://www.jfree.org/jfreechart/";
    public static String protocolDoi = "10.1002/0471142727.mb1422s101";
    
    public static File getPropertiesFile() {
        return new File(System.getProperty("user.home"), ".cellXsettings");
    }
    public CellXParameter membraneLocationParameter;
    public CellXParameter maximumCellLengthParameter;
    public CellXParameter membraneIntensityProfileParameter;
    public CellXParameter membraneWidthParameter;
    public CellXParameter seedRadiusLimitParameter;
    public CellXParameter cropRegionBoundaryParameter;
    public CellXParameter isGraphCutOnCLAHE;
    public CellXParameter useWienerFilter;
    public CellXParameter wiener2params;
    public CellXParameter isNonConvexRegionDetectionEnabled;
    public CellXParameter isHoughTransformOnCLAHE;
    public CellXParameter claheClipLimit;
    public CellXParameter claheBlockSize;
    public static final String CellXConfigurationXMLRootNodeTagName = "CellXConfiguration";
    public static final String CellXConfigurationXMLParamNodeTagName = "CellXParam";
    public static final String CellXConfigurationXMLArrayElementNodeTagName = "ArrayElement";
    public static final String CellXConfigurationXMLAttrName = "name";
    public static final String CellXConfigurationXMLAttrValue = "value";
    public static String CellXFileSeriesXMLRootNodeName = "CellXFileSeries";

    public enum Mode {

        ZOOM_PAN(Color.black),
        MEMBRANE_PROFILE(Color.RED),
        SEED_SIZE(Color.CYAN),
        CELL_LENGTH(Color.GREEN),
        CROP(Color.ORANGE);
        public final Color color;

        private Mode(Color c) {
            this.color = c;
        }
    };
    private final ArrayList<CellXParameter> parameters = new ArrayList<CellXParameter>();
    private final ArrayList<CellXParameter> advancedParameters = new ArrayList<CellXParameter>();
    private final HashMap<String, CellXParameter> name2param = new HashMap<String, CellXParameter>();
    private final HashSet<String> generalParam = new HashSet<String>();
    private static CellXGuiConfiguration instance;

    public static CellXGuiConfiguration getInstance() {
        if (instance == null) {
            instance = new CellXGuiConfiguration();
        }
        return instance;
    }

    public static List<CellXParameter> getParameters() {
        return getInstance().parameters;
    }

    public static List<CellXParameter> getAdvancedParameters() {
        return getInstance().advancedParameters;
    }

    public CellXParameter getParam(String paramName) {
        return name2param.get(paramName);
    }

    public boolean hasParam(String name) {
        return name2param.containsKey(name);
    }

    public boolean isGeneralParameter(String name) {
        return generalParam.contains(name);
    }

    private CellXGuiConfiguration() {
        initParams();
        initAdvancedParams();
    }

    private void initParams() {
        String name = "membraneIntensityProfile";
        CellXParameter cxp = new CellXParameter(name, Double[].class, parameters.size());
        cxp.setDisplayName("Cell Membrane Profile");
        cxp.setToolTipText("The profile of the membrane along a ray that starts inside of the cell and crosses perpendicularly the membrane");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN_ELEMENT_NUM, 4);
        parameters.add(cxp);
        name2param.put(name, cxp);
        membraneIntensityProfileParameter = cxp;
        generalParam.add(name);


        name = "membraneLocation";
        cxp = new CellXParameter(name, Integer.class, parameters.size());
        cxp.setDisplayName("Cell Membrane Location");
        cxp.setToolTipText("The location of the membrane in the profile");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0);
        parameters.add(cxp);
        name2param.put(name, cxp);
        membraneLocationParameter = cxp;
        generalParam.add(name);

        name = "membraneWidth";
        cxp = new CellXParameter(name, Integer.class, parameters.size());
        cxp.setDisplayName("Cell Membrane Width");
        cxp.setToolTipText("The thickness of the membrane");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 1);
        parameters.add(cxp);
        name2param.put(name, cxp);
        membraneWidthParameter = cxp;
        generalParam.add(name);

        name = "maximumCellLength";
        cxp = new CellXParameter(name, Integer.class, parameters.size());
        cxp.setDisplayName("Maximum Cell Length");
        cxp.setToolTipText("The maximum elongation of the largest cell on the image");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 10);
        parameters.add(cxp);
        name2param.put(name, cxp);
        maximumCellLengthParameter = cxp;
        generalParam.add(name);

        name = "seedRadiusLimit";
        cxp = new CellXParameter(name, Integer[].class, parameters.size());
        cxp.setDisplayName("Min and Max Seed Radius");
        cxp.setToolTipText("The maximum radius of a circle that fits into the smallest and the largest cell on the image, respectively");
        cxp.set(CellXParameter.PROPERTY_TYPE.ELEMENT_NUM, 2);
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 3);
        parameters.add(cxp);
        name2param.put(name, cxp);
        seedRadiusLimitParameter = cxp;
        generalParam.add(name);

        name = "cropRegionBoundary";
        cxp = new CellXParameter(name, Integer[].class, parameters.size());
        cxp.setDisplayName("Crop (x, y, width, height)");
        cxp.setToolTipText("If defined, performs the analysis on the selected region only (takes the whole image otherwise)");
        cxp.set(CellXParameter.PROPERTY_TYPE.ELEMENT_NUM, 4);

        cxp.addIdxLimit(CellXParameter.PROPERTY_TYPE.MINEQ, 0, 0);
        cxp.addIdxLimit(CellXParameter.PROPERTY_TYPE.MINEQ, 1, 0);
        cxp.addIdxLimit(CellXParameter.PROPERTY_TYPE.MINEQ, 2, MIN_CROP_WIDTH);
        cxp.addIdxLimit(CellXParameter.PROPERTY_TYPE.MINEQ, 3, MIN_CROP_WIDTH);

        cxp.set(CellXParameter.PROPERTY_TYPE.OPTIONAL, 1);
        parameters.add(cxp);
        name2param.put(name, cxp);
        cropRegionBoundaryParameter = cxp;
        generalParam.add(name);
    }

    private void setParamProperty(String paramName, CellXParameter.PROPERTY_TYPE pt, Object value) {
        name2param.get(paramName).set(pt, value);
    }

    private void initAdvancedParams() {
        
        String name = "isHoughTransformOnCLAHE";
        CellXParameter cxp = new CellXParameter(name, false, advancedParameters.size());
        cxp.setDisplayName("Seeding on CLAHE Image");
        cxp.setToolTipText("If checked, computes the hough transform on the CLAHE image");
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        isHoughTransformOnCLAHE = cxp;

        name = "seedSensitivity";
        cxp = new CellXParameter(name, new Double(0.2), advancedParameters.size());
        cxp.setDisplayName("Seed Detection Cutoff");
        cxp.setToolTipText("Sensitivity of the Hough transform cell center detection");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAX, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);


        name = "isGraphCutOnCLAHE";
        cxp = new CellXParameter(name, false, advancedParameters.size());
        cxp.setDisplayName("Segmentation on CLAHE Image");
        cxp.setToolTipText("If checked, computes the graph cut on the CLAHE image");
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        isGraphCutOnCLAHE = cxp;
        

        name = "claheClipLimit";
        cxp = new CellXParameter(name, new Double(0.01), advancedParameters.size());
        cxp.setDisplayName("CLAHE Clip Limit");
        cxp.setToolTipText("CLAHE histogram clip limit (1 means no clipping, 0 results in the original image)");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAXEQ, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        claheClipLimit = cxp;

        name = "claheBlockSize";
        cxp = new CellXParameter(name, new Integer(100), advancedParameters.size());
        cxp.setDisplayName("CLAHE Blocksize");
        cxp.setToolTipText("Size (in pixels) of the CLAHE tiles (blocksize x blocksize)");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 10);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        claheBlockSize = cxp;
        
        name = "useWienerFilter";
        cxp = new CellXParameter(name, true, advancedParameters.size());
        cxp.setDisplayName("Apply Wiener Filter");
        cxp.setToolTipText("If checked, a Wiener-Filter denoising is applied.");
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        useWienerFilter = cxp;
        
        name = "wiener2params";
        cxp = new CellXParameter(name, new Integer[]{3,3}, advancedParameters.size());
        cxp.setDisplayName("Wiener Filter neighborhood");
        cxp.setToolTipText("Wiener Filter X and Y neighborhood size");
        cxp.set(CellXParameter.PROPERTY_TYPE.ELEMENT_NUM, 2);
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 3);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        wiener2params = cxp;
        
        name = "isNonConvexRegionDetectionEnabled";
        cxp = new CellXParameter(name, false, advancedParameters.size());
        cxp.setDisplayName("Enable Convex Region Detection");
        cxp.setToolTipText("If checked, convex objects can be segmented. Uncheck for round and rod shaped objects.");
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        isNonConvexRegionDetectionEnabled = cxp;

        name = "maximumMinorAxisLengthHoughRadiusRatio";
        cxp = new CellXParameter(name, new Double(1.7), advancedParameters.size());
        cxp.setDisplayName("Maximum Minor Axis Growth");
        cxp.setToolTipText("Accept refined cells if their minor axis length/hough radius ratio is below this value");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);


        name = "requiredFractionOfAcceptedMembranePixels";
        cxp = new CellXParameter(name, new Double(0.85), advancedParameters.size());
        cxp.setDisplayName("Fraction of Good Membrane Pixels");
        cxp.setToolTipText("Accept cell if the fraction of good membrane pixels is above this threshold");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAX, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);


        name = "overlapMergeThreshold";
        cxp = new CellXParameter(name, new Double(0.8), advancedParameters.size());
        cxp.setDisplayName("Merge Threshold");
        cxp.setToolTipText("Merge cell clusters for which the pairwise relative overlap exceeds this value");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAX, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);


        name = "overlapResolveThreshold";
        cxp = new CellXParameter(name, new Double(0.2), advancedParameters.size());
        cxp.setDisplayName("Resolve Threshold");
        cxp.setToolTipText("Resolve cell clusters for which the pairwise relative overlap falls below this value");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAX, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);


        name = "nuclearVolumeFraction";
        cxp = new CellXParameter(name, new Double(0.07), advancedParameters.size());
        cxp.setDisplayName("Nucleus Fraction");
        cxp.setToolTipText("The fraction: nucleus volume/cell volume");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAX, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);

        name = "intensityBrightAreaPercentage";
        cxp = new CellXParameter(name, new Double(0.3), advancedParameters.size());
        cxp.setDisplayName("Bright Area Extent");
        cxp.setToolTipText("Fraction to define the bright pixels (0.3 corresponds to evaluation of pixels above the 70 precentile)");
        cxp.set(CellXParameter.PROPERTY_TYPE.MIN, 0.0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAX, 1.0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);


        name = "spatialMotionFactor";
        cxp = new CellXParameter(name, new Integer(50), advancedParameters.size());
        cxp.setDisplayName("Spatial Cell Mobility");
        cxp.setToolTipText("Maximum movement of cells between consecutive frames");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);

        name = "maxGapConnectionDistance";
        cxp = new CellXParameter(name, new Integer(3), advancedParameters.size());
        cxp.setDisplayName("Track Spanning Distance");
        cxp.setToolTipText("Maximum number of frames a track can span");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        
        name = "fluoQuantiles";
        cxp = new CellXParameter(name, new Double[]{25.0,50.0,75.0}, advancedParameters.size());
        cxp.setDisplayName("Fluorescence Quantiles");
        cxp.setToolTipText("Fluorescence quantiles to report in cell.txt");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 0);
        cxp.set(CellXParameter.PROPERTY_TYPE.MAXEQ, 100);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        
        name = "intensityClassesCount";
        cxp = new CellXParameter(name, new Integer[]{3}, advancedParameters.size());
        cxp.setDisplayName("# Intensity Classes");
        cxp.setToolTipText("Number of intensity classes for gaussian mixture fluorescence background estimate. Can be an interval, e.g., [1, 3]");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 1);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);
        
        name = "fluoAlignPixelMove";
        cxp = new CellXParameter(name, new Integer(1), advancedParameters.size());
        cxp.setDisplayName("Max. FluoAlign Pixel Movement");
        cxp.setToolTipText("Max # of pixels allowed for the fluo-image displacement (in pixels) such that the total intensity of cells is maximized");
        cxp.set(CellXParameter.PROPERTY_TYPE.MINEQ, 0);
        advancedParameters.add(cxp);
        name2param.put(name, cxp);

    }

    public String getAboutDialogMessage() {
        final String name = String.format("CellXGui (%d.%02d)", CellXGuiConfiguration.version, CellXGuiConfiguration.release);
        final StringBuilder sb = new StringBuilder();
        final int indent=12;
        final int rightpadding=20;
        final int sSpace=2;
        final int mSpace=6;
        final int bSpace=20;
        
        sb.append("<body>");
        sb.append("<h1>").append(name).append("</h1>");

        sb.append("<div>Graphical user interface for CellX</div>");
        //sb.append("<div style='height:0px;overflow:hidden;font-size:").append(sSpace).append("px'>&nbsp;</div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(mSpace).append("px'>&nbsp;</div>");   
        final String cellxurl = String.format("<a href=\"%s\">%s</a>", CellXURL, CellXURL);
        sb.append("<div style='margin-left:").append(indent).append("px'>").append(cellxurl).append("</div>");
    
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(bSpace).append("px'>&nbsp;</div>");

        sb.append("<div>The following paper decribes the usage of CellX</div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(mSpace).append("px'>&nbsp;</div>");
        sb.append("<div style='margin-left:").append(indent).append("px'><b><span style='font-size:14.0pt'>Using CellX to Quantify Intracellular Events</span></b></div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(sSpace).append("px'>&nbsp;</div>");
        sb.append("<div style='margin-left:").append(indent).append("px'><i>C Mayer, S Dimopolous, F Rudolf, J Stelling</i></div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(sSpace).append("px'>&nbsp;</div>");
        
                
        final String doiurl = String.format("<a href=\"http://dx.doi.org/%s\">%s</a>",protocolDoi, protocolDoi);
        sb.append("<div style='margin-left:").append(indent).append(String.format("px'>DOI: %s</div>", doiurl));
        
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(mSpace).append("px'>&nbsp;</div>");
        sb.append("<div>Current Maintainer: Lukas Widmer &lt;lukas.widmer@bsse.ethz.ch&gt;<br></div>");
        sb.append("<div>Current Maintainer: Andreas Cuny &lt;andreas.cuny@bsse.ethz.ch&gt;<br></div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(bSpace).append("px'>&nbsp;</div>");
        sb.append("<div>CellXGui uses and appreciates the following libraries:<br>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(mSpace).append("px'>&nbsp;</div>");
       
        final String ijlink = String.format("<a href=\"%s\">%s %s</a>", ImageJURL,"ImageJ",ImageJURL);
        sb.append("<div style='margin-left:").append(indent).append("px'>").append(ijlink).append("</div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(sSpace).append("px'>&nbsp;</div>");
        final String clahelink = String.format("<a href=\"%s\">%s %s</a>", CLAHEURL, "CLAHE plugin of Fiji/ImageJ" , CLAHEURL);
        sb.append("<div style='margin-left:").append(indent).append("px;margin-right:").append(rightpadding).append("px'>").append(clahelink).append("</div>");
        sb.append("<div style='height:0px;overflow:hidden;font-size:").append(sSpace).append("px'>&nbsp;</div>");
        final String freechartlink = String.format("<a href=\"%s\">%s %s</a>", JFreeChartURL, "JFreeChart ", JFreeChartURL);
        sb.append("<div style='margin-left:").append(indent).append("px'>").append(freechartlink).append("</div>");
        sb.append("</div>");
        sb.append("</body>");

        return sb.toString();
    }
}
