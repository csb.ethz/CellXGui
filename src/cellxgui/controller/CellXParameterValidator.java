/*
 * CellXParameterValidator.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller;

import cellxgui.model.CellXModel;
import cellxgui.view.CellXView;
import javax.swing.JOptionPane;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXParameterValidator {

    private final CellXView view;
    private final CellXModel model;

    public CellXParameterValidator(CellXView view, CellXModel model) {
        this.view = view;
        this.model = model;
    }

    public boolean hasUndefinedParam() {
        for (int i = 0; i < model.getParameterValues().length; ++i) {
            if (model.getParameterValues()[i] == null && !CellXGuiConfiguration.getParameters().get(i).has(CellXParameter.PROPERTY_TYPE.OPTIONAL)) {
                final String msg = String.format("Missing value for parameter '%s'", CellXGuiConfiguration.getParameters().get(i).getDisplayName());
                JOptionPane.showMessageDialog(view, msg, "Missing parameter value", JOptionPane.ERROR_MESSAGE);
                return true;
            }
        }
        for (int i = 0; i < model.getAdvancedParameterValues().length; ++i) {
            if (model.getAdvancedParameterValues()[i] == null && !CellXGuiConfiguration.getAdvancedParameters().get(i).has(CellXParameter.PROPERTY_TYPE.OPTIONAL)) {
                final String msg = String.format("Missing value for parameter '%s'", CellXGuiConfiguration.getAdvancedParameters().get(i).getDisplayName());
                JOptionPane.showMessageDialog(view, msg, "Missing parameter value", JOptionPane.ERROR_MESSAGE);
                return true;
            }
        }
        return false;
    }

    public boolean isCompleteForHoughTransform() {
        final int idx = CellXGuiConfiguration.getInstance().seedRadiusLimitParameter.getLocation();
        if (model.getParameterValues()[idx] == null) {
            final String msg = String.format("Missing value for parameter '%s'", CellXGuiConfiguration.getParameters().get(idx).getDisplayName());
            JOptionPane.showMessageDialog(view, msg, "Missing parameter value", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        if (!isValidForCLAHETransform()) {
            return false;
        }
        return true;
    }

    public boolean isValidForCLAHETransform() {
        if (model.isClaheEnabled()) {
            final int blockSizeIdx = CellXGuiConfiguration.getInstance().claheBlockSize.getLocation();
            final int mdh = model.getMinimumImageDim() / 2;

            final Object obj = model.getAdvancedParameterValues()[blockSizeIdx];

            if (obj != null) {
                final int currentBlockSize = (Integer) obj;
                if (currentBlockSize > mdh) {
                    final String msg = String.format("CLAHE blocksize out of range (maximum value: %d)", mdh);
                    JOptionPane.showMessageDialog(view, msg, "Parameter out of range", JOptionPane.ERROR_MESSAGE);
                    return false;
                } else {
                    return true;
                }
            }
            return false;
        }

        return true;
    }

    public boolean isCompleteForSegmentation() {

        if (hasUndefinedParam()) {
            return false;
        }

        //TODO
        //check values

        if (model.getCurrentMembraneLocation() > model.getCurrentMembraneProfile().length) {
            showInfo("Membrane location out of range (must be within the profile)", "Invalid Membrane Location");
            return false;
        }

        return true;
    }

    private void showInfo(String msg, String title) {
        JOptionPane.showMessageDialog(view, msg, title, JOptionPane.INFORMATION_MESSAGE);
    }
}
