/*
 * CellXController.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller;

import cellxgui.controller.filehandling.CellXTimeSeries;
import cellxgui.model.*;
import cellxgui.model.shapes.Coordinates;
import cellxgui.view.BatchProcessPanel;
import cellxgui.view.CellXProjectFileFilter;
import cellxgui.view.CellXView;
import cellxgui.view.ImageDialog;
import cellxgui.view.dialogs.ExecDialog;
import cellxgui.view.dialogs.WorkerDialog;
import cellxgui.workers.*;
import ij.ImagePlus;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXController implements PropertyChangeListener, Runnable {

    public static String IMAGE_PROPERTY = "Image";
    public static String IMAGE_PANEL_MODE = "ImagePanelMode";
    public static String OVERLAY_CHANGE = "OverlayChange";
    public static String NEW_MEMBRANE_PROFILE = "MembraneProfile";
    public static String NEW_SEED_SIZE = "SeedSize";
    public static String NEW_CELL_LENGTH = "CellLength";
    public static String NEW_CROP = "CropRegion";
    public static String LIST_BOX_MODEL = "ListBoxModel";
    public static String SELECTED_SHAPES = "SelectedShapes";
    public static String CELL_X_STATUS = "CellXStatus";
    public static String MEMBRANE_PROFILE = "MembraneProfile";
    public static String PARAM_TABLE_MODEL_CHANGE = "ParamTableModel";
    public static String ADV_PARAM_TABLE_MODEL_CHANGE = "AdvancedParamTableModel";
    public static String MEMBRANE_LOCATION = "MembraneLocation";
    public static String INFO_MESSAGE = "InfoMessage";
    public static String MEMBRANE_WIDTH = "MembraneWidth";
    public static String CLAHE_CHANGE = "ClaheChange";
    public static String OOF_PATTERN = "OofPattern";
    public static String FLUO_PATTERNS = "FluoPatternList";
    public static String DO_CELL_TRACKING = "CellTrackingOption";
    public static String FRAME_IDX_PATTERN = "FramePattern";
    public static String POSITION_PATTERN = "PositionPattern";
    public static String BATCH_IMAGE_DIR = "BatchImageDir";
    public static String FILE_SET_PREVIEW = "FileSetPreview";
    public static String FLAT_FIELD_PATTERN = "FlatFieldPattern";
    public static String ONE_FLATFIELD_PER_FRAME = "OneFlatFieldImagePerFrame";
    public static String FLUO_IMAGE_CHANGED = "FluoImageChanged";
    public static String FLAT_FIELD_IMAGE_CHANGED = "FlatFieldImageChanged";
    public static String BATCH_RESULTS_DIR = "BatchResultsDir";
    private CellXModel model;
    private BatchProcessModel batchProcessModel;
    private final CellXView view;
    private final BatchProcessPanel batchView;
    private final CellXGuiProperties properties;
    private ImagePlus currentRawImagePlus;
    private boolean isModified = false;

    public CellXController() {
        this.properties = new CellXGuiProperties();
        this.batchView = new BatchProcessPanel(this);
        this.model = new CellXModel(this);
        this.batchProcessModel = new BatchProcessModel(this);
        this.batchProcessModel.initFromPropertiesFile(properties);
        this.view = new CellXView(this, batchView);
        this.model.fireAllProperties();
        this.batchProcessModel.fireAllProperties();
        this.properties.fireAll();
    }

    @Override
    public void run() {
        try {
            final URL url = ClassLoader.getSystemResource("cellxgui/csb_ball.png");
            final Image icon = new ImageIcon(url).getImage();
            view.setIconImage(icon);
        } catch (Exception e) {
        }
        view.setVisible(true);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        view.modelPropertyChange(evt);
        batchView.modelPropertyChange(evt);
    }

    public void setCurrentImageFileName(File f) {
        //this.currentImageFileName = f;
        model.setCurrentImage(f);
    }

    public void setCurrentImagePlus(ImagePlus ip) {
        this.currentRawImagePlus = ip;
    }

    public void setModel(CellXModel model) {
        this.model.removeAllPropertyChangeListener();
        this.model = model;
        this.model.setController(this);
        this.model.fireAllProperties();
        this.isModified = false;
    }

    public void setSelectedShapes(int[] selectedIndices) {
        model.setSelectedShapes(selectedIndices);
    }

    public void setImagePanelMode(CellXGuiConfiguration.Mode mode) {
        model.setImagePanelMode(mode);
    }

    public void setCropRegion(Coordinates mpc) {
        this.isModified = true;
        model.setCrop(mpc);
    }

    public void setAdvancedParameter(Object value, int row) {
        this.isModified = true;
        model.updateAdvancedParameter(value, row);
    }

    public void setParameter(Object value, int row) {
        this.isModified = true;
        model.updateParameter(value, row);
    }

    public void setMembraneLocation(int pos) {
        this.isModified = true;
        model.updateMembraneLocation(pos);
    }

    public void setMembraneWidth(int width) {
        this.isModified = true;
        model.updateMembraneWidth(width);
    }

    public void setMembraneProfileRegion(int beginIdx, int endIdx) {
        this.isModified = true;
        model.updateMembraneProfileRegion(beginIdx, endIdx);
    }

    public void setBatchImageDirectory(File selectedFile) {
        this.isModified = true;
        batchProcessModel.setBatchImageDirectory(selectedFile);
    }

    public void setOutOfFocusPattern(Pattern p) {
        this.isModified = true;
        batchProcessModel.setOutOfFocusPattern(p);
    }

    public void setBatchTrackingEnabled(boolean selected) {
        this.isModified = true;
        batchProcessModel.setTrackingEnabled(selected);
    }

    public void setFrameIdxPattern(Pattern p) {
        this.isModified = true;
        batchProcessModel.setFrameIdxPattern(p);
    }

    public void setFlatFieldPattern(Pattern p) {
        this.isModified = true;
        batchProcessModel.setFlatFieldPattern(p);
    }

    public void setBatchModel(BatchProcessModel newBatchModel) {
        this.batchProcessModel.removeAllPropertyChangeListener();
        this.batchProcessModel = newBatchModel;
        this.batchProcessModel.setController(this);
        this.batchProcessModel.fireAllProperties();
    }

    public void setFluoImage(File f) {
        //propertyChange(new PropertyChangeEvent(this, FLUO_IMAGE_CHANGED, this.fluoImage == null ? "" : this.fluoImage.toString(), f == null ? "" : f.toString()));
        //this.fluoImage = f;
        model.setFluoTestImage(f);
    }

    public void setFlatFieldImage(File f) {
        // propertyChange(new PropertyChangeEvent(this, FLAT_FIELD_IMAGE_CHANGED, this.flatFieldImage == null ? "" : this.flatFieldImage.toString(), f == null ? "" : f.toString()));
        // this.flatFieldImage = f;
        model.setFlatFieldTestImage(f);
    }

    public void setModified() {
        isModified = true;
    }

    /*
     * ADD
     */
    public void addFluoTypePattern(Pattern p) {
        this.isModified = true;
        batchProcessModel.addFluoTypePattern(p);
    }

    public void addCellLength(Coordinates mpc) {
        this.isModified = true;
        model.addCellLength(mpc);
    }

    public void addMembraneProfile(Coordinates mpc) {
        this.isModified = true;
        model.addMembraneProfile(mpc);
    }

    public void addSeedSize(Coordinates mpc) {
        this.isModified = true;
        model.addSeedSize(mpc);
    }

    /*
     * DELETE
     */
    public void deleteSelectedShapes(int[] selectedIndices) {
        this.isModified = true;
        model.deleteSelectedShapes(selectedIndices);
    }

    public void deleteSelectedFluoPatterns(int[] selectedIndices) {
        this.isModified = true;
        batchProcessModel.deleteFluoPatterns(selectedIndices);
    }

    /*
     * AUTO
     */
    public void autoMembraneIntensityProfile() {
        this.isModified = true;
        model.autoMembraneIntensityProfile();
    }

    public void autoMembraneLocation() {
        this.isModified = true;
        model.autoMembraneLocation();
    }

    public void autoMembraneWidth() {
        this.isModified = true;
        model.autoMembraneWidth();
    }

    public void autoSeedRadiusLimit() {
        this.isModified = true;
        model.autoSeedRadiusLimit();
    }

    public void autoCropRegion() {
        this.isModified = true;
        model.autoCropRegion();
    }

    public void autoMaximumCellLength() {
        this.isModified = true;
        model.autoMaximumCellLength();
    }


    /*
     * REVERT
     */
    public void revertFlatFieldPattern() {
        batchProcessModel.fireFlatFieldPattern();
    }

    public void revertFrameIdxPattern() {
        batchProcessModel.fireFrameIdxPattern();
    }

    public void revertOutOfFocusPattern() {
        batchProcessModel.fireOutOFFocusPattern();
    }

    public void revertBatchImageDir() {
        batchProcessModel.fireImageDir();
    }

    public void resetMembraneProfilePanelData() {
        model.resetMembraneProfilePanelData();
    }

    /*
     * GETTERS
     */
    public CellXModel getModel() {
        return model;
    }

    public CellXView getView() {
        return view;
    }

    public ImagePlus getImagePlus() {
        return this.currentRawImagePlus;
    }

    public CellXGuiProperties getProperties() {
        return properties;
    }

    public File getTmpResultSubDir() {
        return new File(properties.getTmpDir(), getBaseName(model.getCurrentImage()));
    }

    public static String getBaseName(File f) {
        String baseName = f.getName();
        baseName = baseName.substring(0, baseName.lastIndexOf('.'));
        return baseName;
    }

    public List<Pattern> getBatchFluorescencePatterns() {
        return batchProcessModel.getFluoPatterns();
    }

    /*
     * CHECKS/ACTIONS
     */
    public void createNewModelWithImagePlus(ImagePlus ip, File f) {
        this.currentRawImagePlus = ip;
        final CellXModel newModel = new CellXModel();
        newModel.setImage(ip, f);
        this.setModel(newModel);
        this.isModified = true;
    }

    public void createEmptyProject() {
        if (replaceModelIfModified()) {
            setModel(new CellXModel());
            //this.currentImageFileName = null;
            //this.fluoImage = null;
            //this.flatFieldImage = null;
            this.batchProcessModel = new BatchProcessModel(this);
            this.batchProcessModel.initFromPropertiesFile(properties);
            this.batchProcessModel.fireAllProperties();
            showNotificationMessage("Created new project");
        }
    }

    public boolean hasInvalidParameter() {
        CellXParameterValidator v = new CellXParameterValidator(view, model);
        if (v.hasUndefinedParam()) {
            return true;
        }
        return false;
    }

    public void updateCLAHEMode() {
        if (currentRawImagePlus != null) {
            CellXParameterValidator v = new CellXParameterValidator(view, model);
            if (v.isValidForCLAHETransform()) {
                final ClaheImageWorker ciw = new ClaheImageWorker(this);
                final WorkerDialog bd = new WorkerDialog();
                bd.setLocationRelativeTo(view);
                bd.execute(ciw, "Updating source image ");
            } else {
                model.updateAdvancedParameter(false, CellXGuiConfiguration.getInstance().isGraphCutOnCLAHE.getLocation());
            }
        }
    }

    public void runCellXHoughTransform() {
        if (!validCurrentImageFile()) {
            return;
        }

        if (!validCellXBatch()) {
            return;
        }

        if (!validMCR()) {
            return;
        }

        if (!new CellXParameterValidator(view, model).isCompleteForHoughTransform()) {
            return;
        }

        final File tmpDir = getTmpResultSubDir();

        try {
            tmpDir.mkdirs();
            tmpDir.deleteOnExit();
            final File tmpConfigFile = File.createTempFile("cellXConfig", ".xml", tmpDir);
            saveConfigToXML(tmpConfigFile);
            tmpConfigFile.deleteOnExit();

            final File tmpControlImgFile = File.createTempFile("cellXControlImg", ".png", tmpDir);
            tmpControlImgFile.deleteOnExit();
            final String[] cmd = CellXCommandBuilder.getHoughTransformCmd(properties, tmpConfigFile, model.getCurrentImage(), tmpControlImgFile);
            ExecDialog ed = new ExecDialog(this, false, "CellX", "Running CellX test seeding ...");
            ed.executeCalibrateWorker(cmd, tmpControlImgFile, null, false);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(view, "Cannot create tmp file\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    public void runCellXSegmentation() {

        if (!validCurrentImageFile()) {
            return;
        }

        if (!validCellXBatch()) {
            return;
        }

        if (!validMCR()) {
            return;
        }


        if (!validOptionalFiles()) {
            return;
        }

        if (!new CellXParameterValidator(view, model).isCompleteForSegmentation()) {
            return;
        }

        final File tmpDir = getTmpResultSubDir();
        try {
            tmpDir.mkdirs();
            tmpDir.deleteOnExit();
            final File tmpConfigFile = File.createTempFile("cellXConfig", ".xml", tmpDir);
            saveConfigToXML(tmpConfigFile);
            tmpConfigFile.deleteOnExit();
            final File tmpControlImgFile = File.createTempFile("controlCellX", ".png", tmpDir);
            tmpControlImgFile.deleteOnExit();
            File tmpFluoControlImgFile = null;
            if (model.getFluoTestImage() != null) {
                tmpFluoControlImgFile = File.createTempFile("fluoControlCellX", ".png", tmpDir);
                tmpFluoControlImgFile.deleteOnExit();
            }
            final String[] cmd = CellXCommandBuilder.getTestSegmentationCmd(properties, tmpConfigFile, model.getCurrentImage(), tmpControlImgFile, model.getFluoTestImage(), tmpFluoControlImgFile, model.getFlatFieldTestImage());
            final ExecDialog ed = new ExecDialog(this, false, "CellX", "Running CellX test segmentation ...");
            ed.executeCalibrateWorker(cmd, tmpControlImgFile, tmpFluoControlImgFile, true);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(view, "Error: Tmp file creation\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    public void generateFileSeries() {
        FileSetWorker fw = new FileSetWorker(properties, batchProcessModel, this);
        final WorkerDialog bd = new WorkerDialog();
        bd.setLocationRelativeTo(view);
        bd.execute(fw, "Generating file series");
    }

    public void runBatch(int[] selectedIndices) {


        if (!validCellXBatch()) {
            return;
        }

        if (!validMCR()) {
            return;
        }


        if (!validOptionalFiles()) {
            return;
        }

        if (!new CellXParameterValidator(view, model).isCompleteForSegmentation()) {
            return;
        }

        final File resultsDir = batchProcessModel.getBatchResultsDir();

        final File configFile = new File(resultsDir, CellXGuiConfiguration.DEFAULT_CALIBRATION_FILE_NAME);

        saveConfigToXML(configFile);


        final File fileSeriesFile = new File(resultsDir, CellXGuiConfiguration.DEFAULT_FILE_SERIES_FILE_NAME);

        saveFileSeriesToXML(fileSeriesFile, selectedIndices);

        final String[] cmd = CellXCommandBuilder.getBatchCommand(properties, configFile, resultsDir, fileSeriesFile);
        final ExecDialog ed = new ExecDialog(this, false, "CellX", "Running CellX ...");
        ed.executeBatchWorker(cmd);
    }

    public void close() {
        if (isModified) {
            int aw = JOptionPane.showConfirmDialog(view, "This project was not saved.\nSave now?", "Save project?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (aw == JOptionPane.YES_OPTION) {
                final JFileChooser jfc = new JFileChooser();
                jfc.setMultiSelectionEnabled(false);
                final CellXProjectFileFilter cxff = new CellXProjectFileFilter();
                jfc.addChoosableFileFilter(cxff);
                jfc.setFileFilter(jfc.getChoosableFileFilters()[1]);
                final int ret = jfc.showSaveDialog(view);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File f = jfc.getSelectedFile();
                    if (!f.toString().endsWith(CellXProjectFileFilter.FILE_EXTENSION) && jfc.getFileFilter() == cxff.getFileFilter()) {
                        f = new File(f.toString() + CellXProjectFileFilter.FILE_EXTENSION);
                    }
                    if (f.exists()) {
                        aw = JOptionPane.showConfirmDialog(view, "The file '" + f.toString() + "' exists, do you want to overwrite it?", "File exists", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                        if (aw != JOptionPane.OK_OPTION) {
                            return;
                        }
                    }
                    saveProject(f);
                } else {
                    return;
                }
            }
        }

        //list running threads
        /*System.out.println("Current thread pool:\n");
        ThreadGroup root = Thread.currentThread().getThreadGroup().getParent();
        while (root.getParent() != null) {
        root = root.getParent();
        }
        recCollectThreads(root, 0);
         */
        view.dispose();
    }

    /*
     * SHOW
     */
    public void showErrorMessage(String title, String message) {
        JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public void showBatchPanel() {
        view.showBatchPanelTab();
    }

    public void showNotificationMessage(String format) {
        view.setNotificationMsg(format);
    }

    private boolean replaceModelIfModified() {
        if (isModified) {
            final int aw = JOptionPane.showConfirmDialog(view, "This project is not saved.\nDo you really want to replace it? ", "Change Project ", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (aw != JOptionPane.OK_OPTION) {
                return false;
            }
        }
        return true;
    }

    public void showControlImage(BufferedImage img, Point p) {
        final ImageDialog imgViewer = new ImageDialog(view, false, img);
        imgViewer.setTitle("CellX control image");
        imgViewer.setSize(CellXGuiConfiguration.IMAGE_VIEWER_WIDTH, CellXGuiConfiguration.IMAGE_VIEWER_HEIGHT);
        imgViewer.setLocationRelativeTo(view);
        if (p != null) {
            imgViewer.setLocation(p.x, p.y);
        }
        imgViewer.setVisible(true);
    }
    /*
     * SAVE
     */

    public void saveProject(File f) {
        try {
            if (batchView.commitForSave()) {
                return;
            }

            final FileOutputStream fos = new FileOutputStream(f);
            final ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(model.getCurrentImage());
            int width = 0, height = 0;
            if (currentRawImagePlus != null) {
                width = currentRawImagePlus.getWidth();
                height = currentRawImagePlus.getHeight();
            }
            oos.writeInt(width);
            oos.writeInt(height);
            oos.writeObject(model.getFluoTestImage());
            oos.writeObject(model.getFlatFieldTestImage());
            oos.writeObject(model.getParameterValues());
            oos.writeObject(model.getAdvancedParameterValues());
            oos.writeObject(model.getCellLengthCoordinates());
            oos.writeObject(model.getCropRegionCoordinates());
            oos.writeObject(model.getMembraneProfileCoordinates());
            oos.writeObject(model.getSeedSizeCoordinates());
            oos.writeObject(batchProcessModel);
            fos.close();
            showNotificationMessage(String.format("Wrote '%s'", f));
            isModified = false;
            properties.setProjectDir(f.getParentFile());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(view, "IOError: Cannot save project\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void saveFileSeriesToXML(File file, CellXTimeSeries ts) {
        final TimeSeriesXMLWriter w = new TimeSeriesXMLWriter();
        w.writeOneSeriesToFile(file, ts);
        if (w.wasSuccessful()) {
            view.setNotificationMsg("Wrote '" + file.toString() + "'");
        }
    }

    public void saveFileSeriesToXML(File file, int[] selected) {
        final TimeSeriesXMLWriter w = new TimeSeriesXMLWriter();
        w.writeSelectedSeriesToFile(file, batchProcessModel.getTimeSeriesList(), selected);
        if (w.wasSuccessful()) {
            view.setNotificationMsg("Wrote '" + file.toString() + "'");
        }
    }

    public void saveConfigToXML(File file) {
        final ConfigurationXMLWriter w = new ConfigurationXMLWriter(model);
        w.writeToFile(file);
        if (w.wasSuccessful()) {
            view.setNotificationMsg("Wrote '" + file.toString() + "'");
        }
    }

    public void saveFileSeriesToXML(File file, int idx) {
        final CellXTimeSeries serie = batchProcessModel.getFileSeries(idx);
        saveFileSeriesToXML(file, serie);
    }

    /*
     * LOAD
     */
    public void loadImage(final File f) {
        if (replaceModelIfModified()) {
            final ImageLoader wd = new ImageLoader(f, this);
            final WorkerDialog bd = new WorkerDialog();
            bd.setLocationRelativeTo(view);
            bd.execute(wd, "Loading image '" + f.toString() + "'");
            properties.setProjectDir(f.getParentFile());
        }
    }

    public void loadProject(final File f) {
        if (replaceModelIfModified()) {
            final ProjectLoader pl = new ProjectLoader(f, this);
            final WorkerDialog bd = new WorkerDialog();
            bd.setLocationRelativeTo(view);
            bd.execute(pl, "Loading project '" + f.toString() + "'");
            properties.setProjectDir(f.getParentFile());
        }
    }

    public void loadWithRelocatedImage(File projectFile, String msg) {
        File relocatedImage;
        final int relocate = JOptionPane.showConfirmDialog(view, msg + "\nDo you want to set the new location of the segmentation image?", "Calibration Image Changed", JOptionPane.YES_NO_OPTION);
        if (relocate == 0) {
            final JFileChooser jfc = new JFileChooser();
            jfc.setMultiSelectionEnabled(false);
            final int ret = jfc.showOpenDialog(view);
            if (ret == JFileChooser.APPROVE_OPTION) {
                relocatedImage = jfc.getSelectedFile();
            } else {
                return;
            }
            final ProjectLoader pl2 = new ProjectLoader(projectFile, this, relocatedImage);
            final WorkerDialog bd2 = new WorkerDialog();
            bd2.setLocationRelativeTo(view);
            bd2.execute(pl2, "Loading project '" + projectFile.toString() + "'");
        }
    }

    public void loadAndShowControlImage(File file) {
        final ControlImageLoader cil = new ControlImageLoader(file, this);
        final WorkerDialog bd = new WorkerDialog();
        bd.setLocationRelativeTo(view);
        bd.execute(cil, "Loading control image '" + file.toString() + "'");
    }

    /*
     * STATIC
     */
    //for debugging
    private static void recCollectThreads(ThreadGroup group, int level) {
        final Thread[] threads = new Thread[group.activeCount() * 10];
        for (int i = 0; i < group.enumerate(threads, false); ++i) {
            System.out.println(String.format("L:%3d  %s", level, threads[i]));
        }
        final ThreadGroup[] groups = new ThreadGroup[group.activeGroupCount() * 10];
        for (int i = 0; i < group.enumerate(groups, false); ++i) {
            recCollectThreads(groups[i], level + 1);
        }
    }

    public void setBatchResultsDirectory(File selectedFile) {
        batchProcessModel.setBatchResultsDirectory(selectedFile);
    }

    public boolean validCellXBatch() {
        if (properties.getCellXBatch() == null) {
            JOptionPane.showMessageDialog(view, "CellX executable missing!\nPlease set the CellX executable in File -> Settings.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        final File f = new File(properties.getCellXBatch());
        if (!f.exists()) {
            JOptionPane.showMessageDialog(view, "CellX executable '" + f.toString() + "' does not exist!\nPlease set the CellX executable in File -> Settings", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }


        return true;
    }

    public boolean validMCR() {
        if (System.getProperty("os.name").startsWith("Windows")) {
            return true;
        }

        if (properties.getMCR() == null) {
            JOptionPane.showMessageDialog(view, "Matlab Compiler Runtime missing!\nPlease set the MCR location in File -> Settings", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        final File f = new File(properties.getMCR());
        if (!f.exists()) {
            JOptionPane.showMessageDialog(view, "Matlab Compiler Runtime '" + f.toString() + "' does not exist!\nPlease set the MCR location in File -> Settings", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        return true;
    }

    private boolean validOptionalFiles() {
        if (!view.commitFluoFile()) {
            return false;
        }
        if (!view.commitFlatFieldFile()) {
            return false;
        }
        return true;
    }

    private boolean validCurrentImageFile() {
        if (model.getCurrentImage() == null) {
            JOptionPane.showMessageDialog(view, "No image loaded", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public void resetFileSeries() {
        batchProcessModel.resetFileSeries();
    }

    public File getCurrentImage() {
        return model.getCurrentImage();
    }

    public void setMembraneProfile(Double[] newValue) {
        model.setMembraneProfile(newValue);
    }

    public void importParameters(File selectedFile) {
        try {
            CellXImportParameters.importCellXParameters(this, selectedFile);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(view, "Cannot parse " + selectedFile.toString() + "'", "IO Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, "Cannot import" + selectedFile.toString() + "'", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public BatchProcessModel getBatchModel() {
        return batchProcessModel;
    }
}
