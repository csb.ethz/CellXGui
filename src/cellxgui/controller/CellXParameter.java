/*
 * CellXParameter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXParameter {

    private final String name;
    private final Class clazz;
    private final int location;
    private String toolTipText = "";
    private String displayName = "";

    public enum PROPERTY_TYPE {

        DEFAULT_VALUE,
        MIN,
        MAX,
        ELEMENT_NUM,
        MIN_ELEMENT_NUM,
        MAX_ELEMENT_NUM,
        FIXED_VALUE_SET,
        OPTIONAL,
        MINEQ, //value must be larger or equal to this value
        MAXEQ,// calue must smaller or equal to this value
        LIMIT_FOR_IDX
    }
    private final EnumMap<PROPERTY_TYPE, Object> map = new EnumMap<PROPERTY_TYPE, Object>(PROPERTY_TYPE.class);

    public CellXParameter(String name, Class clazz, int loc) {
        this.name = name;
        this.clazz = clazz;
        this.location = loc;
    }

    public CellXParameter(String name, Object defaultvalue, int loc) {
        this(name, defaultvalue.getClass(), loc);
        set(PROPERTY_TYPE.DEFAULT_VALUE, defaultvalue);
    }

    public final void set(PROPERTY_TYPE pt, Object value) {
        map.put(pt, value);
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setToolTipText(String toolTipText) {
        this.toolTipText = toolTipText;
    }

    public String getDisplayName() {
        if (!displayName.isEmpty()) {
            return displayName;
        }
        return name;
    }

    public String getToolTipText() {
        return toolTipText;
    }

    public String getName() {
        return name;
    }

    public Object get(PROPERTY_TYPE pt) {
        return map.get(pt);
    }

    public Object getDefaultValue() {
        return get(PROPERTY_TYPE.DEFAULT_VALUE);
    }

    public int getLocation() {
        return location;
    }

    public boolean isNumericValueClass() {
        return (clazz.equals(Double.class) || clazz.equals(Integer.class));
    }

    public Class getValueClass() {
        return clazz;
    }

    public boolean isArray() {
        return (clazz.equals(Double[].class) || clazz.equals(Integer[].class));
    }

    public boolean isFile() {
        return clazz.equals(File.class);
    }

    public boolean isInteger() {
        return clazz.equals(Integer.class);
    }

    public boolean isIntegerArray() {
        return clazz.equals(Integer[].class);
    }

    public boolean isDoubleArray() {
        return clazz.equals(Double[].class);
    }

    public boolean isBoolean() {
        return clazz.equals(Boolean.class);
    }

    public boolean has(PROPERTY_TYPE pROPERTY_TYPE) {
        return map.containsKey(pROPERTY_TYPE);
    }

    public boolean isDouble() {
        return clazz.equals(Double.class);
    }

    @Override
    public String toString() {
        return name;
    }

    public void setMaximum(int i) {
        map.put(PROPERTY_TYPE.MAXEQ, i);
        final Number def = (Number) map.get(PROPERTY_TYPE.DEFAULT_VALUE);
        if (def.intValue() > i) {
            map.put(PROPERTY_TYPE.DEFAULT_VALUE, new Integer(i));
        }

    }

    public void addIdxLimit(PROPERTY_TYPE type, int idx, Number value) {

        Map<Integer, List<Limit>> index2limit;

        if (has(PROPERTY_TYPE.LIMIT_FOR_IDX)) {
            index2limit = (Map<Integer, List<Limit>>) get(PROPERTY_TYPE.LIMIT_FOR_IDX);
        } else {
            index2limit = new HashMap<Integer, List<Limit>>();
            map.put(PROPERTY_TYPE.LIMIT_FOR_IDX, index2limit);
        }

        if (index2limit.containsKey(idx)) {
            index2limit.get(idx).add(new Limit(type, value));
        } else {
            final List<Limit> tmp = new ArrayList<Limit>();
            tmp.add(new Limit(type, value));
            index2limit.put(idx, tmp);
        }
    }

    public List<Limit> getLimitsForIdx(int idx) {
        if (has(PROPERTY_TYPE.LIMIT_FOR_IDX)) {
            final Map<Integer, List<Limit>> index2limit = (Map<Integer, List<Limit>>) get(PROPERTY_TYPE.LIMIT_FOR_IDX);
            return index2limit.get(idx);
        } else {
            return null;
        }
    }

    public static class Limit {

        private final PROPERTY_TYPE type;
        private final Number limit;

        public Limit(PROPERTY_TYPE type, Number limit) {
            this.type = type;
            this.limit = limit;
        }

        public Number getLimit() {
            return limit;
        }

        public PROPERTY_TYPE getType() {
            return type;
        }
    }
}
