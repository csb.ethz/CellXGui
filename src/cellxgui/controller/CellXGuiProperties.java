/*
 * CellXGuiProperties.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXGuiProperties extends Properties {

    private final File propertiesFile;
    public static final String MCR_PROPERTY = "MCR";
    public static final String CELLX_BATCH_BINARY = "CELLX_BATCH_BINARY";
    public static final String TMP_DIR_PROPERTY = "TMP_DIR";
    public static final String IMAGE_DIR_PROPERTY = "IMG_DIR";
    public static final String PROJECT_DIR_PROPERTY = "PROJECT_DIR";
    public static final String OOF_PATTERN = "OOF_PATTERN";
    public static final String FRAME_PATTERN = "FRAME_PATTERN";
    public static final String POSITION_PATTERN = "POSITION_PATTERN";
    public static final String BATCH_IMAGE_DIR = "BATCH_IMG_DIR";
    public static final String FLUO_PATTERN = "FLUO_PATTERN";
    public static final String FLAT_FIELD_PATTERN = "FLAT_FIELD_PATTERN";
    public static final String ONE_FLAT_FIELD_IMAGE_PER_FRAME = "ONE_FLAT_FIELD_IMAGE_PER_FRAME";
    public static final String TRACKING_ENABLED = "TRACKING_ENABLED";
    private final List<String> fluoPatternList = new ArrayList<String>();
    private final PropertyChangeSupport propertyChangeSupport;

    public CellXGuiProperties() {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.propertiesFile = CellXGuiConfiguration.getPropertiesFile();
        try {
            if (propertiesFile.exists()) {
                load(new FileInputStream(propertiesFile));
                initFluoList();
            }
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Cannot load properties file {0}", e.getMessage());
        }
        setDefaults();
    }

    private void setDefaults() {
        if (getTmpDir() == null) {
            setTmpDir(System.getProperty("java.io.tmpdir"));
        }
    }

    public void fireAll() {
        mcrChanged();
        tmpDirChanged();
        cellXBatchChanged();
    }

    public String getMCR() {
        return getProperty(MCR_PROPERTY);
    }

    public void setMCR(String v) {
        setProperty(MCR_PROPERTY, v);
        store();
        propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(this, MCR_PROPERTY, null, v));
    }

    private void mcrChanged() {
        propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(this, MCR_PROPERTY, null, getProperty(MCR_PROPERTY)));
    }

    public String getCellXBatch() {
        return getProperty(CELLX_BATCH_BINARY);
    }

    public void setCellXBatch(String v) {
        setProperty(CELLX_BATCH_BINARY, v);
        store();
        cellXBatchChanged();
    }

    private void cellXBatchChanged() {
        propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(this, CELLX_BATCH_BINARY, null, getProperty(CELLX_BATCH_BINARY)));
    }

    public void setTmpDir(String v) {
        setProperty(TMP_DIR_PROPERTY, v);
        store();
        tmpDirChanged();
    }

    private void tmpDirChanged() {
        propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(this, TMP_DIR_PROPERTY, null, getProperty(TMP_DIR_PROPERTY)));
    }

    public String getTmpDir() {
        return getProperty(TMP_DIR_PROPERTY);
    }

    public File getImageDir() {
        if (containsKey(IMAGE_DIR_PROPERTY)) {
            return new File(getProperty(IMAGE_DIR_PROPERTY));
        } else {
            return null;
        }
    }

    public void setImageDir(String v) {
        setProperty(IMAGE_DIR_PROPERTY, v);
        store();
        imgDirChanged();
    }

    private void imgDirChanged() {
        propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(this, IMAGE_DIR_PROPERTY, null, getProperty(IMAGE_DIR_PROPERTY)));
    }

    public void store() {
        try {
            store(new FileOutputStream(propertiesFile), null);
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Cannot save properties file {0}", e.getMessage());
        }
    }

    public File getPropertiesFile() {
        return propertiesFile;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    private void initFluoList() {
        String pName = String.format("%s%d", FLUO_PATTERN, fluoPatternList.size());
        while (containsKey(pName)) {
            fluoPatternList.add(getProperty(pName));
            pName = String.format("%s%d", FLUO_PATTERN, fluoPatternList.size());
        }
    }

    public void setFluoPatterns(List<Pattern> fluoPatterns) {
        for (int i = 0; i < fluoPatternList.size(); ++i) {
            final String pName = String.format("%s%d", FLUO_PATTERN, i);
            remove(pName);
        }
        fluoPatternList.clear();
        for (Pattern p : fluoPatterns) {
            final String pName = String.format("%s%d", FLUO_PATTERN, fluoPatternList.size());
            put(pName, p.toString());
            fluoPatternList.add(p.toString());
        }
        store();
    }

    public List<String> getFluoPatterns() {
        return fluoPatternList;
    }

    public void setOOFPattern(Pattern p) {
        if (p == null) {
            return;
        }
        put(OOF_PATTERN, p.toString());
        store();
    }

    public void setFramePattern(Pattern p) {
        if (p == null) {
            return;
        }
        put(FRAME_PATTERN, p.toString());
        store();
    }

    public void setPositionPattern(Pattern p) {
        if (p == null) {
            return;
        }
        put(POSITION_PATTERN, p.toString());
        store();
    }

    public void setBatchImageDir(File dir) {
        if (dir == null) {
            return;
        }
        put(BATCH_IMAGE_DIR, dir.toString());
        store();
    }

    public boolean containsBatchImageDir() {
        return containsKey(BATCH_IMAGE_DIR);
    }

    public File getBatchImageDir() {
        return new File(getProperty(BATCH_IMAGE_DIR));
    }

    public boolean containsOOFPattern() {
        return containsKey(OOF_PATTERN);
    }

    public Pattern getOOFPattern() {
        return Pattern.compile(getProperty(OOF_PATTERN));
    }

    public boolean containsFluoPatterns() {
        return !fluoPatternList.isEmpty();
    }

    public boolean containsFramePattern() {
        return containsKey(FRAME_PATTERN);
    }

    public Pattern getFramePattern() {
        return Pattern.compile(getProperty(FRAME_PATTERN));
    }

    public boolean containsPositionPattern() {
        return containsKey(POSITION_PATTERN);
    }

    public Pattern getPositionPattern() {
        return Pattern.compile(getProperty(POSITION_PATTERN));
    }

    public void setFlatFieldPattern(Pattern p) {
        if (p == null) {
            return;
        }
        put(FLAT_FIELD_PATTERN, p.toString());
        store();
    }

    public boolean containsFlatFieldPattern() {
        return containsKey(FLAT_FIELD_PATTERN);
    }

    public Pattern getFlatFieldPattern() {
        return Pattern.compile(getProperty(FLAT_FIELD_PATTERN));
    }

    public void setOneFlatFieldPerFrame(boolean oneFlatFieldImagePerFrame) {
        put(ONE_FLAT_FIELD_IMAGE_PER_FRAME, oneFlatFieldImagePerFrame ? "true" : "false");
        store();
    }

    public void setTrackingEnabled(boolean b) {
        put(TRACKING_ENABLED, b ? "true" : "false");
        store();
    }

    public boolean containsTrackingEnabled() {
        return containsKey(TRACKING_ENABLED);
    }

    public boolean isTrackingEnabled() {
        return getProperty(TRACKING_ENABLED).equals("true");
    }

    public boolean containsOneFlatFieldImagePerFrame() {
        return containsKey(ONE_FLAT_FIELD_IMAGE_PER_FRAME);
    }

    public boolean isOneFlatFieldImagePerFrame() {
        return getProperty(ONE_FLAT_FIELD_IMAGE_PER_FRAME).equals("true");
    }

    public boolean hasImageDir() {
        return containsKey(IMAGE_DIR_PROPERTY);
    }

    public boolean hasProjectDir() {
        return containsKey(PROJECT_DIR_PROPERTY);
    }

    public File getProjectDir() {
        return new File(getProperty(PROJECT_DIR_PROPERTY));
    }

    void setProjectDir(File parentFile) {
        setProperty(PROJECT_DIR_PROPERTY, parentFile.toString());
        store();
    }
}
