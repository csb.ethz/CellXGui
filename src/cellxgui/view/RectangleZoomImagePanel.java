/*
 * RectangleZoomImagePanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 * 
 * replaced by ImagePanel
 */
@Deprecated
public final class RectangleZoomImagePanel extends JPanel {

    protected final BufferedImage image;
    protected double sx1, sx2, sy1, sy2, tx1, tx2, ty1, ty2, currentScale;
    protected OrderedRectangle mouseRectangle;
    protected final double imageHeight, imageWidth;
    protected Color backgroundColor = Color.darkGray;
    protected Color rectangleColor = Color.green;

    @Deprecated
    public RectangleZoomImagePanel(BufferedImage image) {
        this.image = image;
        imageHeight = image.getHeight();
        imageWidth = image.getWidth();
        sx1 = 0;
        sy1 = 0;
        sx2 = image.getWidth();
        sy2 = image.getHeight();

        initMouse();
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(backgroundColor);
        g.fillRect(0, 0, getWidth(), getHeight());
        final Graphics2D g2 = (Graphics2D) g;
        computeTransform();
        //System.out.println(String.format("Destination: x1:%5d, y1:%5d x2:%5d y2:%5d \n", (int) tx1, (int) ty1, (int) tx2, (int) ty2));
        //System.out.println(String.format("Source     : x1:%5d, y1:%5d x2:%5d y2:%5d \n", (int) sx1, (int) sy1, (int) sx2, (int) sy2));
        g2.drawImage(image, (int) tx1, (int) ty1, (int) tx2, (int) ty2, (int) Math.floor(sx1), (int) (int) Math.floor(sy1), (int) (int) Math.ceil(sx2), (int) (int) Math.ceil(sy2), this);
        if (mouseRectangle != null) {
            g2.setColor(rectangleColor);
            mouseRectangle.paint(g2);
        }
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setRectangleColor(Color rectangleColor) {
        this.rectangleColor = rectangleColor;
    }

    public void resetZoom() {
        sx1 = 0;
        sy1 = 0;
        sx2 = image.getWidth();
        sy2 = image.getHeight();
    }

    public void updateZoomRegion(OrderedRectangle canvasRectangle) {
        final Point2D.Double ul = canvas2image(canvasRectangle.x1, canvasRectangle.y1);
        final Point2D.Double lr = canvas2image(canvasRectangle.x2, canvasRectangle.y2);
        updateZoomRegionOnImage(ul.x, ul.y, lr.x, lr.y);
    }

    private void updateZoomRegionOnImage(double x1, double y1, double x2, double y2) {
        sx1 = Math.max(x1, 0.0);
        sy1 = Math.max(y1, 0.0);
        sx2 = Math.min(x2, imageWidth);
        sy2 = Math.min(y2, imageHeight);
        //System.out.println(String.format("Source     : x1:%5d, y1:%5d x2:%5d y2:%5d \n", (int) sx1, (int) sy1, (int) sx2, (int) sy2));
    }

    public void drag(double dxCanvas, double dyCanvas) {
        final double dix = dxCanvas / currentScale;
        final double diy = dyCanvas / currentScale;
        double nx1, nx2, ny1, ny2;
        nx1 = sx1 + dix;
        nx2 = sx2 + dix;
        ny1 = sy1 + diy;
        ny2 = sy2 + diy;
        if (nx1 < 0.0 || nx2 > imageWidth) {
            nx1 = sx1;
            nx2 = sx2;
        }
        if (ny1 < 0.0 || ny2 > imageHeight) {
            ny1 = sy1;
            ny2 = sy2;
        }
        updateZoomRegionOnImage(nx1, ny1, nx2, ny2);
    }

    private void computeTransform() {
        final double displayRatio = getWidth() / (double) getHeight();
        final double sourceWidth = (sx2 - sx1);
        final double sourceHeight = (sy2 - sy1);
        final double imgRatio = sourceWidth / sourceHeight;
        if (displayRatio > imgRatio) {
            //scale to fill height
            currentScale = getHeight() / sourceHeight;
            final double w = sourceWidth * currentScale;
            ty1 = 0;
            ty2 = getHeight();
            tx1 = (getWidth() - w) / 2;
            tx2 = tx1 + w;

        } else {
            //scale to fill width
            currentScale = getWidth() / sourceWidth;
            final double h = sourceHeight * currentScale;
            tx1 = 0.0;
            tx2 = getWidth();
            ty1 = (getHeight() - h) / 2;
            ty2 = ty1 + h;
        }
    }

    public Point2D.Double canvas2image(double x1, double y1) {
        final double x = ((x1 - tx1) / currentScale) + sx1;
        final double y = ((y1 - ty1) / currentScale) + sy1;
        return new Point2D.Double(x, y);
    }

    public boolean isWithinImage(Point2D.Double p) {
        if (p.x < 0.0) {
            return false;
        }
        if (p.x >= image.getWidth()) {
            return false;
        }
        if (p.y < 0.0) {
            return false;
        }
        if (p.y >= image.getHeight()) {
            return false;
        }
        return true;
    }

    public Point2D.Double limitToImage(Point2D.Double p) {

        double x = p.x, y = p.y;

        if (x < 0.0) {
            x = 0.0;
        }
        if (x > image.getWidth()) {
            x = image.getWidth();
        }
        if (y < 0.0) {
            y = 0.0;
        }
        if (y > image.getHeight()) {
            y = image.getHeight();
        }
        return new Point2D.Double(x, y);
    }

    public OrderedRectangle transformToImageRectangle(OrderedRectangle canvas) {
        final Point2D.Double ul = canvas2image(canvas.x1, canvas.y1);
        final Point2D.Double lr = canvas2image(canvas.x2, canvas.y2);
        return new OrderedRectangle(ul.x, ul.y, lr.x, lr.y);


    }

    public void setMouseRectangle(OrderedRectangle mouseRectangle) {
        this.mouseRectangle = mouseRectangle;
    }

    public void removeMouseRectangle() {
        this.mouseRectangle = null;
    }

    protected void initMouse() {
        final MouseAdapter ma = new RZIPMouseAdapter(this);
        this.addMouseListener(ma);
        this.addMouseMotionListener(ma);
    }

    public static class OrderedRectangle {

        public final double x1, y1, x2, y2;

        public OrderedRectangle(double x1, double y1, double x2, double y2) {
            if (x1 < x2) {
                this.x1 = x1;
                this.x2 = x2;
            } else {
                this.x1 = x2;
                this.x2 = x1;
            }
            if (y1 < y2) {
                this.y1 = y1;
                this.y2 = y2;
            } else {
                this.y1 = y2;
                this.y2 = y1;
            }
        }

        public double getWidth() {
            return x2 - x1;
        }

        public double getHeight() {
            return y2 - y1;
        }

        public void paint(Graphics2D g2) {
            g2.drawRect((int) x1, (int) y1, (int) getWidth(), (int) getHeight());
        }
    }

    public static class RZIPMouseAdapter extends MouseAdapter {

        private final RectangleZoomImagePanel rzip;
        private Point mouseDown;
        private Point previousPoint;
        private int mouseDownButton;

        public RZIPMouseAdapter(RectangleZoomImagePanel rzip) {
            this.rzip = rzip;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            final Point2D.Double p = rzip.canvas2image(e.getX(), e.getY());
            if (rzip.isWithinImage(p)) {
                mouseDown = e.getPoint();
                mouseDownButton = e.getButton();
            } else {
                mouseDown = null;
                rzip.removeMouseRectangle();
            }
            previousPoint = e.getPoint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {

            if (mouseDown != null && mouseDownButton == MouseEvent.BUTTON1) {
                rzip.setMouseRectangle(new OrderedRectangle(mouseDown.x, mouseDown.y, e.getX(), e.getY()));
            }
            if (mouseDownButton == MouseEvent.BUTTON3) {
                rzip.drag(previousPoint.getX() - e.getX(), previousPoint.getY() - e.getY());
                previousPoint = e.getPoint();
            }
            rzip.repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (mouseDown != null) {
                if (mouseDownButton == MouseEvent.BUTTON1) {
                    final OrderedRectangle r = new OrderedRectangle(mouseDown.x, mouseDown.y, e.getX(), e.getY());
                    final OrderedRectangle imgR = rzip.transformToImageRectangle(r);
                    if (imgR.getHeight() > 3 && imgR.getWidth() > 3) {
                        rzip.updateZoomRegion(r);
                    }
                }
            }
            rzip.removeMouseRectangle();
            rzip.repaint();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() > 1) {
                rzip.resetZoom();
            }
            if (e.getButton() == MouseEvent.BUTTON3 || e.isControlDown()) {
                rzip.resetZoom();
            }
            rzip.repaint();
        }
    }
}
