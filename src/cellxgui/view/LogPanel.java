/*
 * LogPanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class LogPanel extends JTextPane {

    private final SimpleAttributeSet stderrAttr;
    private final SimpleAttributeSet stdoutAttr;

    public LogPanel() {
        stderrAttr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(stderrAttr, "Monospaced");
        StyleConstants.setFontSize(stderrAttr, 10);
        stdoutAttr = new SimpleAttributeSet(stderrAttr);
        StyleConstants.setForeground(stderrAttr, Color.red);
        StyleConstants.setForeground(stdoutAttr, Color.green);
        setEditable(false);
        this.setBackground(new Color(0, 0, 0, 0));
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        super.paintComponent(g);
    }

    public void appendStderr(String msg) {
        try {
            this.getStyledDocument().insertString(getStyledDocument().getLength(), msg + "\n", stderrAttr);
        } catch (Exception e) {
        }
    }

    public void appendStdout(String msg) {
        try {
            this.getStyledDocument().insertString(getStyledDocument().getLength(), msg + "\n", stdoutAttr);
        } catch (Exception e) {
        }
    }
}
