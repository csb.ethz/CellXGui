/*
 * AbstractView.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
*/
package cellxgui.view;

import java.beans.PropertyChangeEvent;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public interface AbstractView {  
    public void modelPropertyChange(PropertyChangeEvent evt);  
}
