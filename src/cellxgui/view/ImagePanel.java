/*
 * ImagePanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 */
package cellxgui.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ImagePanel extends JPanel {

    protected AffineTransform a;
    protected final BufferedImage image;
    Point2D upperLeft, lowerRight;
    protected OrderedRectangle mouseRectangle;
    protected Color mouseRectangleColor = new Color(200, 255, 210);
    protected Color backgroundColor = Color.darkGray;
    protected Color textColor = Color.lightGray;
    protected boolean showHelp;
    protected String[] help = new String[]{
        "right mouse click / ctrl + left mouse click  : reset           ",
        "left mouse pressed + drag                    : select zoom area",
        "right mouse pressed + drag / ctrl + drag     : move image"
    };
    protected int fontsize = 12;

    public ImagePanel(BufferedImage img) {
        this.image = img;
        upperLeft = new Point2D.Double(0, 0);
        lowerRight = new Point2D.Double(img.getWidth(), img.getHeight());
    }

    public void initMouse() {
        MouseControl mc = new MouseControl(this);
        addMouseListener(mc);
        addMouseMotionListener(mc);
        showHelp = true;
    }

    public void setShowHelp(boolean showHelp) {
        this.showHelp = showHelp;
    }

    public void setHelp(String[] help) {
        this.help = help;
    }

    @Override
    public void paintComponent(Graphics g) {
        final Graphics2D g2 = (Graphics2D) g;
        g2.setColor(backgroundColor);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(textColor);


        if (showHelp) {
            renderHelp(g2);
        }

        g2.drawImage(image, a, this);
        if (mouseRectangle != null) {
            g2.setColor(mouseRectangleColor);
            g2.draw(mouseRectangle);
        }
    }

    @Override
    public void doLayout() {
        super.doLayout();
        a = initAffineTransform();
    }

    public void drag(double dxCanvas, double dyCanvas) {
        try {
            final Point2D d = a.createInverse().deltaTransform(new Point2D.Double(dxCanvas, dyCanvas), null);
            upperLeft.setLocation(upperLeft.getX() - d.getX(), upperLeft.getY() - d.getY());
            lowerRight.setLocation(lowerRight.getX() - d.getX(), lowerRight.getY() - d.getY());
            a = initAffineTransform();
        } catch (Exception e) {
        }
        repaint();
    }

    public void updateZoomRegion(OrderedRectangle r) {
        try {
            final Point2D p1 = a.inverseTransform(new Point2D.Double(r.x1, r.y1), null);
            final Point2D p2 = a.inverseTransform(new Point2D.Double(r.x2, r.y2), null);
            if (p1.distance(p2) > 10) {
                upperLeft = p1;
                lowerRight = p2;
            }
        } catch (Exception e) {
        }
        a = initAffineTransform();
    }

    public void resetZoom() {
        upperLeft = new Point2D.Double(0, 0);
        lowerRight = new Point2D.Double(image.getWidth(), image.getHeight());
        a = initAffineTransform();
    }

    public AffineTransform initAffineTransform() {
        final double displayRatio = getWidth() / (double) getHeight();
        final double sourceHeight = lowerRight.getY() - upperLeft.getY();
        final double sourceWidth = lowerRight.getX() - upperLeft.getX();
        final double imgRatio = sourceWidth / sourceHeight;
        double scale = 0.0;
        double xoffset = 0.0;
        double yoffset = 0.0;

        if (displayRatio > imgRatio) {
            //scale to fill height
            scale = getHeight() / sourceHeight;
            final double sw = sourceWidth * scale;
            if (sw < getWidth()) {
                xoffset = (getWidth() - sw) / 2.0;
            }
        } else {
            //scale to fill width
            scale = getWidth() / sourceWidth;
            final double sh = sourceHeight * scale;
            if (sh < getHeight()) {
                yoffset = (getHeight() - sh) / 2.0;
            }
        }

        if (upperLeft != null) {
            xoffset -= scale * upperLeft.getX();
            yoffset -= scale * upperLeft.getY();
        }
        final AffineTransform ret = new AffineTransform(scale, 0, 0, scale, xoffset, yoffset);
        return ret;
    }

    private void setMouseRectangle(OrderedRectangle orderedRectangle) {
        mouseRectangle = orderedRectangle;
    }

    private void renderHelp(Graphics2D g2) {
        Font f = new Font(Font.MONOSPACED, Font.PLAIN, fontsize);


        g2.setFont(f);
        float x = getWidth() / 2;
        float y = getHeight() / 2;

        y -= Math.round(help.length / 2.0) * 1.5 * fontsize;
        x -= g2.getFontMetrics().stringWidth(help[0]) / 2.0;

        for (int i = 0; i < help.length; ++i) {
            g2.drawString(help[i], x, y);
            y += 1.5 * fontsize;
        }


    }

    public static class MouseControl extends MouseAdapter {

        public Point down;
        public Point other;
        private Point previous;
        public final ImagePanel ip;
        int btn;

        public MouseControl(ImagePanel ip) {
            this.ip = ip;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON3 || e.isControlDown()) {
                down = null;
                ip.resetZoom();
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (!e.isControlDown()) {
                btn = e.getButton();
            } else {
                btn = MouseEvent.BUTTON3;
            }
            down = e.getPoint();
            previous = e.getPoint();
            ip.repaint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (btn == MouseEvent.BUTTON1) {
                other = e.getPoint();
                ip.setMouseRectangle(new OrderedRectangle(down.x, down.y, other.x, other.y));
            } else if (btn == MouseEvent.BUTTON3) {
                ip.drag(e.getX() - previous.x, e.getY() - previous.y);
            }
            previous = e.getPoint();
            ip.repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            other = e.getPoint();
            if (down != null && btn == MouseEvent.BUTTON1) {

                ip.updateZoomRegion(new OrderedRectangle(down.x, down.y, other.x, other.y));

            }
            ip.setMouseRectangle(null);
            ip.repaint();
        }
    }

    public static class OrderedRectangle extends Rectangle2D.Double {

        protected double x1, x2, y1, y2;

        public OrderedRectangle(double x1, double y1, double x2, double y2) {
            super(x1 < x2 ? x1 : x2, y1 < y2 ? y1 : y2, Math.abs(x2 - x1), Math.abs(y2 - y1));
            if (x1 < x2) {
                this.x1 = x1;
                this.x2 = x2;
            } else {
                this.x1 = x2;
                this.x2 = x1;
            }
            if (y1 < y2) {
                this.y1 = y1;
                this.y2 = y2;
            } else {
                this.y1 = y2;
                this.y2 = y1;
            }
        }
    }
}
