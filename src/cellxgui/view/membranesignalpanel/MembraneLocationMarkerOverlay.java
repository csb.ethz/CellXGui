package cellxgui.view.membranesignalpanel;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.OverlayChangeListener;
import org.jfree.chart.panel.Overlay;
import org.jfree.ui.RectangleEdge;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembraneLocationMarkerOverlay implements Overlay {

    private final List<OverlayChangeListener> listeners = new ArrayList<OverlayChangeListener>();
    private double membraneLocation = 0;
    private int membraneWidth = 0;
    private String label = "Membrane location";
    private static final int margin = 4;

    public MembraneLocationMarkerOverlay() {
        super();
    }

    @Override
    public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
        //super.paintOverlay(g2, chartPanel);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        final ValueAxis xAxis = chartPanel.getChart().getXYPlot().getDomainAxis();
        final RectangleEdge edge = chartPanel.getChart().getXYPlot().getDomainAxisEdge();

        if (membraneLocation < xAxis.getLowerBound()) {
            return;
        }

        if (membraneLocation > xAxis.getUpperBound()) {
            return;
        }

        final int x = (int) xAxis.valueToJava2D(membraneLocation, chartPanel.getScreenDataArea(), edge);
        final int h = (int) chartPanel.getScreenDataArea().getHeight();


        //final Shape drawShape = shape.createTransformedShape(new AffineTransform(1,0,0, chartPanel.getScreenDataArea().getHeight() ,x1, chartPanel.getScreenDataArea().getY() ));
        g2.translate(x, chartPanel.getScreenDataArea().getY());


        g2.drawLine(-1, 0, -1, h);
        g2.drawLine(1, 0, 1, h);


        final int w2 = (int) (0.5 * membraneWidth * chartPanel.getScreenDataArea().getWidth() / xAxis.getRange().getLength());
        final int h2 = h / 2;
        g2.drawLine(-w2, h2, -1, h2);
        g2.drawLine(1, h2, w2, h2);
        g2.drawLine(-w2, h2 - 2, -w2, h2 + 2);
        g2.drawLine(w2, h2 - 2, w2, h2 + 2);

        // draw label box

        final Rectangle2D box = g2.getFontMetrics().getStringBounds(label, g2);

        g2.translate(1, h - 1);

        final int xb = (int) box.getX();
        final int yb = (int) box.getY() - margin - 1;
        final int wb = (int) box.getWidth() + margin;
        final int hb = (int) box.getHeight() + margin;
        g2.setColor(Color.lightGray);
        g2.fillRect(xb, yb, wb, hb);
        g2.setColor(Color.black);
        g2.drawString(label, margin / 2, -margin / 2);
        g2.drawRect(xb, yb, wb, hb);
    }

    @Override
    public void addChangeListener(OverlayChangeListener ol) {
        listeners.add(ol);
    }

    @Override
    public void removeChangeListener(OverlayChangeListener ol) {
        listeners.remove(ol);
    }

    public void setMembraneLocation(double v) {
        this.membraneLocation = v;
        //    fireMembraneLocationChanged();
    }

    public void setMembraneWidth(int w) {
        this.membraneWidth = w;
        //   fireMembraneLocationChanged();
    }

}
