package cellxgui.view.membranesignalpanel;

import org.jfree.chart.labels.CrosshairLabelGenerator;
import org.jfree.chart.plot.Crosshair;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembraneLabelCrossHairGenerator implements CrosshairLabelGenerator {

    @Override
    public String generateLabel(Crosshair crshr) {
        return "Membrane location";
    }
}
