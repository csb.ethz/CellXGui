/*
 * MembranePanelMouseAdapter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.membranesignalpanel;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXParameter.PROPERTY_TYPE;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import org.jfree.chart.axis.NumberAxis;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembranePanelMouseAdapter extends MouseAdapter {

    private final CellXController control;
    private final MembraneChartPanel mcp;
    private boolean isDrag = false;
    private Point mouseDownPoint;

    public MembranePanelMouseAdapter(CellXController control, MembraneChartPanel mcp) {
        this.control = control;
        this.mcp = mcp;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        final int pos = (int) Math.round(mcp.getChart().getXYPlot().getDomainAxis().java2DToValue(e.getX(), mcp.getScreenDataArea(), mcp.getChart().getXYPlot().getDomainAxisEdge()));
        control.setMembraneLocation(pos);
        isDrag = false;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mouseDownPoint = e.getPoint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        if (isDrag && e.getButton() == MouseEvent.BUTTON1) {
            if (e.getPoint().x < mouseDownPoint.x) {
                control.resetMembraneProfilePanelData();
                isDrag = false;
                return;
            }
            if (e.getPoint().y < mouseDownPoint.y) {
                control.resetMembraneProfilePanelData();
                isDrag = false;
                return;
            }

            if (mcp.getChart().getXYPlot().getDataset() == null) {
                isDrag = false;
                return;
            }
            final NumberAxis xAxis = (NumberAxis) mcp.getChart().getXYPlot().getDomainAxis();
            final double d = xAxis.getRange().getLength();

            final Number minL = (Number) CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.get(PROPERTY_TYPE.MIN_ELEMENT_NUM);
            int minENum = 1;
            if (minL != null) {
                minENum = minL.intValue();
            }
            if (d < minENum) {
                JOptionPane.showMessageDialog(mcp, "Minimum length of the membrane profile is " + minL.intValue());
                mcp.restoreAutoBounds();
            } else {
                mcp.restoreAutoRangeBounds();

                control.setMembraneProfileRegion(
                        (int) Math.round(xAxis.getRange().getLowerBound()),
                        (int) Math.round(xAxis.getRange().getUpperBound()) + 1);
            }
        }
        isDrag = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        isDrag = true;
    }
}
