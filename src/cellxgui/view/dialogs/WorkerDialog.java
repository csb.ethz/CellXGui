package cellxgui.view.dialogs;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class WorkerDialog extends JDialog implements PropertyChangeListener, ActionListener {

    public final static String DONE = "DONE";
    public final static String FAILED = "FAILED";
    public final static String STATUS_MSG = "STATUS_MSG";
    private final JTextArea textComp;
    private final JButton cancelBtn;
    private final JProgressBar progressBar;
    private SwingWorker worker;
    private boolean finished;

    public WorkerDialog() {
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setResizable(false);
        this.setModal(true);
        final JPanel p = new JPanel();
        this.add(p);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        textComp = new JTextArea("Thread running");
        textComp.setEditable(false);
        textComp.setBackground(p.getBackground());
        textComp.setRequestFocusEnabled(false);
        textComp.setMargin(new Insets(5, 5, 5, 5));

        p.add(textComp);

        this.progressBar = new JProgressBar();
        this.progressBar.setIndeterminate(true);
        p.add(progressBar);

        this.cancelBtn = new JButton("Cancel");
        p.add(cancelBtn);


        this.pack();
        initListeners();
    }

    public void execute(SwingWorker worker, String message) {
        finished = false;
        textComp.setText(message);
        this.validate();
        this.worker = worker;
        this.worker.addPropertyChangeListener(this);
        this.worker.execute();
        this.setVisible(true);

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(DONE)) {
            finished = true;
            dispose();
        } else if (evt.getPropertyName().equals(FAILED)) {
            finished = true;
            dispose();
        } else if (evt.getPropertyName().equals(STATUS_MSG)) {
            progressBar.setStringPainted(true);
            progressBar.setString((String) evt.getNewValue());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //System.out.println("Stop thread request");
        if (worker != null) {
            this.worker.cancel(true);
        }
        this.finished = true;
        dispose();
    }

    private void initListeners() {
        this.cancelBtn.addActionListener(this);
    }

    public boolean isFinished() {
        return finished;
    }
}
