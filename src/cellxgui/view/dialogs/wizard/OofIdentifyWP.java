package cellxgui.view.dialogs.wizard;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class OofIdentifyWP extends AbstractSelectionWizardPanel {

    private final OofWizard wizard;

    public OofIdentifyWP(OofWizard wizard) {
        this.wizard = wizard;
        setText("\n\nSelect the part of the filename that identifies out of focus images:");
        validate();
    }

    @Override
    public boolean validData() {
        if (textField.getSelectedText() == null) {
            showErrorMessage("Selection required");
            return false;
        }

        return true;
    }

    @Override
    public void commitData() {
        wizard.setOofIdentifyRegion(textField.getSelectionStart(), textField.getSelectionEnd());
    }

    @Override
    public void beforeShowing() {
        hideErrorMessage();
    }

    @Override
    public void afterShowing() {
        textField.requestFocus();
    }
}
