/*
 * CaptureFluoTypeWizardPanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FlatFieldExtractFluoWP extends AbstractSelectionWizardPanel {

    private final FlatFieldWizard wizard;

    public FlatFieldExtractFluoWP(FlatFieldWizard wizard) {
        this.wizard = wizard;
        setText("\n\nSelect the part of the filename that encodes the corresponding fluorescence type:");
    }

    @Override
    public boolean validData() {
        if (textField.getSelectedText() == null) {
            showErrorMessage("Nothing selected");
            return false;
        }
        return true;
    }

    @Override
    public void commitData() {
        wizard.setFluorescenceTypeRegion(textField.getSelectionStart(), textField.getSelectionEnd());
    }

    @Override
    public void beforeShowing() {
        hideErrorMessage();
    }

    @Override
    public void afterShowing() {
        textField.requestFocus();
    }
}
