package cellxgui.view.dialogs.wizard;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class NoGroupInRegexpException extends Exception {

    private final String pattern;

    public NoGroupInRegexpException(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
