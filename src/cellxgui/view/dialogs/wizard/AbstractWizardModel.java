/*
 * AbstractWizardModel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

import cellxgui.model.AbstractModel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
abstract public class AbstractWizardModel extends AbstractModel {

    public static String FILENAME_PROPERTY = "FileNameProperty";
}
