/*
 * FlatFieldRegexpWizardModel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FlatFieldModel extends AbstractWizardModel {

    private String filename;
    private int fluoTypeStart, fluoTypeEnd, flatFieldStart, flatFieldEnd;
    private final String fluoGroup;

    public FlatFieldModel(String fluoGroup) {
        this.fluoGroup = fluoGroup;
    }

    void setFileName(String name) {
        firePropertyChange(FILENAME_PROPERTY, filename, name);
        filename = name;
    }

    void setFluorescenceTypeRegion(int selectionStart, int selectionEnd) {
        fluoTypeStart = selectionStart;
        fluoTypeEnd = selectionEnd;
    }

    void setFlatFieldIdentifyReagion(int selectionStart, int selectionEnd) {
        flatFieldStart = selectionStart;
        flatFieldEnd = selectionEnd;
    }

    boolean isRegionOverlap(int selectionStart, int selectionEnd) {
        if (selectionStart <= fluoTypeStart) {
            if (selectionEnd > fluoTypeStart) {
                return true;
            } else {
                return false;
            }
        }
        if (selectionStart > fluoTypeStart) {
            if (selectionStart < fluoTypeEnd) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public Pattern generatePattern() {

        StringBuilder pat = new StringBuilder();
        if (fluoTypeStart < flatFieldStart) {
            if (fluoTypeStart == 0) {
                pat.append("^");
            } else {
                pat.append(".+?");
            }
            pat.append(fluoGroup);
            if (fluoTypeEnd != flatFieldStart) {
                pat.append(".+?");
            }
            pat.append(filename.substring(flatFieldStart, flatFieldEnd));

            final int dotIdx = filename.lastIndexOf('.');
            if (dotIdx < 0 || dotIdx < flatFieldEnd) {
                pat.append(".*");
            } else {
                pat.append(".*?").append(OofWizard.getExtensionPattern(filename));
            }

            return Pattern.compile(pat.toString());

        } else {
            if (flatFieldStart == 0) {
                pat.append("^");
            } else {
                pat.append(".+?");
            }
            pat.append(filename.substring(flatFieldStart, flatFieldEnd));
            if (flatFieldEnd != fluoTypeStart) {
                pat.append(".+?");
            }
            pat.append(fluoGroup);

            final int dotIdx = filename.lastIndexOf('.');
            if (dotIdx < 0 || dotIdx < fluoTypeEnd) {
                pat.append(".*");
            } else {
                pat.append(".*?").append(OofWizard.getExtensionPattern(filename));
            }

            return Pattern.compile(pat.toString());
        }


    }
}
