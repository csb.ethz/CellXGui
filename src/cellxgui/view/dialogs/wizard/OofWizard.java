package cellxgui.view.dialogs.wizard;

import cellxgui.view.CellXView;
import java.io.File;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class OofWizard extends Wizard implements WizardWithFilename {

    private String filename;
    private int selectionStart, selectionEnd;

    public OofWizard(CellXView view, File imageDir) {
        super(view);
        initWizardPanels(imageDir);
    }

    private void initWizardPanels(File imageDir) {
        final SelectFileWP p1 = new SelectFileWP(this) {

            @Override
            public boolean validData() {
                if (!super.validData()) {
                    return false;
                }
                if (!textField.getText().matches(".*?\\d+.*")) {
                    showErrorMessage("Filename must contain the frame number");
                    return false;
                }
                return true;
            }
        };
        p1.setFileDialogDirectory(imageDir);
        p1.setText("Enter a out of focus image filename:");
        final OofIdentifyWP p2 = new OofIdentifyWP(this);
        p1.setNextWizardPanel(p2);
        p2.setPeviousWizardPanel(p1);
        registerWizardPanel(p2);
        setInitialPanel(p1);
    }

    @Override
    protected void registerWizardPanel(WizardPanel wp) {
        super.registerWizardPanel(wp);
        addPropertyChangeListener(wp);
    }

    @Override
    public void setFileName(String name) {
        firePropertyChange(FlatFieldModel.FILENAME_PROPERTY, filename, name);
        this.filename = name;
    }

    public void setOofIdentifyRegion(int selectionStart, int selectionEnd) {
        this.selectionStart = selectionStart;
        this.selectionEnd = selectionEnd;
    }

    public Pattern getPattern() {
        return Pattern.compile(generateIdentifyPattern(filename, selectionStart, selectionEnd));
    }

    public static String generateIdentifyPattern(String filename, int selectionStart, int selectionEnd) {
        String selected = filename.substring(selectionStart, selectionEnd);
        selected = selected.replaceAll("\\.", "\\\\.");
        StringBuilder strPattern = new StringBuilder();
        if (selectionStart == 0) {
            strPattern.append("^");
        } else {
            strPattern.append(".*?");
        }
        strPattern.append(selected);
        if (selectionEnd == filename.length()) {
            strPattern.append("$");
        } else {
            final int dotIdx = filename.lastIndexOf('.');
            if (dotIdx < 0 || dotIdx < selectionEnd) {
                strPattern.append(".*");
            } else {
                strPattern.append(".*?").append(getExtensionPattern(filename));
            }
        }
        return strPattern.toString();
    }

    public static String getExtensionPattern(String fname) {
        final int idx = fname.lastIndexOf('.');
        if (idx > -1) {
            return "\\." + fname.substring(idx + 1);
        }
        return "";
    }
}
