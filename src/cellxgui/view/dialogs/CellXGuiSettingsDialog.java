/*
 * CellXGuiSettingsDialog.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */

/*
 * CellXGuiSettingsDialog.java
 *
 * Created on Jan 24, 2012, 1:02:02 PM
 */
package cellxgui.view.dialogs;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXGuiProperties;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXGuiSettingsDialog extends javax.swing.JDialog implements PropertyChangeListener {

    private final CellXGuiProperties properties;

    /** Creates new form CellXGuiSettingsDialog */
    public CellXGuiSettingsDialog(java.awt.Frame parent, boolean modal, CellXGuiProperties properties) {
        super(parent, modal);
        initComponents();
        this.properties = properties;
        this.setResizable(false);
        propertiesFileLabel.setText(CellXGuiConfiguration.getPropertiesFile().toString());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        mcrTextField = new javax.swing.JTextField();
        closeBtn = new javax.swing.JButton();
        mcrBtn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        tmpDirTextField = new javax.swing.JTextField();
        tmpDirBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        batchCellXTF = new javax.swing.JTextField();
        batchCellXBtn = new javax.swing.JButton();
        propertiesFileLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Matlab Compiler Runtime");

        mcrTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mcrTextFieldActionPerformed(evt);
            }
        });

        closeBtn.setText("Apply");
        closeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeBtnActionPerformed(evt);
            }
        });

        mcrBtn.setText("Select");
        mcrBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mcrBtnActionPerformed(evt);
            }
        });

        jLabel3.setText("TMP Directory");

        tmpDirTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tmpDirTextFieldActionPerformed(evt);
            }
        });

        tmpDirBtn.setText("Select");
        tmpDirBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tmpDirBtnActionPerformed(evt);
            }
        });

        jLabel4.setText("CellX Executable");

        batchCellXTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchCellXTFActionPerformed(evt);
            }
        });

        batchCellXBtn.setText("Select");
        batchCellXBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchCellXBtnActionPerformed(evt);
            }
        });

        propertiesFileLabel.setForeground(new java.awt.Color(153, 153, 153));
        propertiesFileLabel.setText("Properties");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 555, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(mcrTextField)
                                    .addComponent(tmpDirTextField)
                                    .addComponent(jLabel1)
                                    .addComponent(batchCellXTF))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(batchCellXBtn)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(tmpDirBtn)
                                        .addComponent(mcrBtn)))))
                        .addGap(42, 42, 42))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(301, 301, 301)
                        .addComponent(closeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(propertiesFileLabel)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mcrTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mcrBtn))
                .addGap(55, 55, 55)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(batchCellXTF, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(batchCellXBtn))
                .addGap(45, 45, 45)
                .addComponent(jLabel3)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tmpDirBtn)
                    .addComponent(tmpDirTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                .addComponent(closeBtn)
                .addGap(11, 11, 11)
                .addComponent(propertiesFileLabel))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mcrTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mcrTextFieldActionPerformed
        // TODO add your handling code here:
        properties.setMCR(mcrTextField.getText());
    }//GEN-LAST:event_mcrTextFieldActionPerformed

    private void mcrBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mcrBtnActionPerformed
        // TODO add your handling code here:
        final File f = incFile("Select MCR directory", true, null);
        if (f != null) {
            properties.setMCR(f.toString());
        }
    }//GEN-LAST:event_mcrBtnActionPerformed

    private void tmpDirTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tmpDirTextFieldActionPerformed
        // TODO add your handling code here:
        properties.setTmpDir(tmpDirTextField.getText());
    }//GEN-LAST:event_tmpDirTextFieldActionPerformed

    private void tmpDirBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tmpDirBtnActionPerformed
        // TODO add your handling code here:

        final File f = incFile("Select directory for temporary files", true, null);
        if (f != null) {
            properties.setTmpDir(f.getAbsolutePath());
        }
    }//GEN-LAST:event_tmpDirBtnActionPerformed

    private void closeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeBtnActionPerformed
        // TODO add your handling code here:      
        if (!mcrTextField.getText().trim().isEmpty()) {
            properties.setMCR(mcrTextField.getText());
        }
        if (!tmpDirTextField.getText().trim().isEmpty()) {
            properties.setTmpDir(tmpDirTextField.getText());
        }
        if (!batchCellXTF.getText().trim().isEmpty()) {
            properties.setCellXBatch(batchCellXTF.getText());
        }
        this.dispose();
    }//GEN-LAST:event_closeBtnActionPerformed

    private void batchCellXTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batchCellXTFActionPerformed
        // TODO add your handling code here:
        properties.setCellXBatch(batchCellXTF.getText());
    }//GEN-LAST:event_batchCellXTFActionPerformed

    private void batchCellXBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batchCellXBtnActionPerformed

        File initDir = null;
        if (properties.getCellXBatch() != null) {
            initDir = new File(properties.getCellXBatch());
            if (initDir.exists()) {
                initDir = initDir.getParentFile();
            } else {
                initDir = null;
            }
        }
        final File f = incFile("Select CellX batch executable", false, initDir);
        if (f != null) {
            properties.setCellXBatch(f.getAbsolutePath());
        }
    }//GEN-LAST:event_batchCellXBtnActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton batchCellXBtn;
    private javax.swing.JTextField batchCellXTF;
    private javax.swing.JButton closeBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JButton mcrBtn;
    private javax.swing.JTextField mcrTextField;
    private javax.swing.JLabel propertiesFileLabel;
    private javax.swing.JButton tmpDirBtn;
    private javax.swing.JTextField tmpDirTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(CellXGuiProperties.MCR_PROPERTY)) {
            if (evt.getNewValue() != null) {
                mcrTextField.setText((String) evt.getNewValue());
            }
        } else if (evt.getPropertyName().equals(CellXGuiProperties.CELLX_BATCH_BINARY)) {
            if (evt.getNewValue() != null) {
                batchCellXTF.setText((String) evt.getNewValue());
            }
        } else if (evt.getPropertyName().equals(CellXGuiProperties.TMP_DIR_PROPERTY)) {
            if (evt.getNewValue() != null) {
                tmpDirTextField.setText((String) evt.getNewValue());
            }
        }
    }

    private File incFile(String title, boolean dir, File init) {
        final JFileChooser jfc = new JFileChooser();
        jfc.setApproveButtonText("Set");
        jfc.setDialogTitle(title);
        if (dir) {
            jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            jfc.setAcceptAllFileFilterUsed(false);
        }

        if (init != null && init.exists()) {
            if (init.isFile() && dir) {
                jfc.setCurrentDirectory(init.getParentFile());
            } else {
                jfc.setSelectedFile(init);
            }
        }

        final int r = jfc.showOpenDialog(this);
        if (r == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        }
        return null;
    }

    @Override
    public void setVisible(boolean b) {
        if (System.getProperty("os.name").startsWith("Windows")) {
            mcrTextField.setEnabled(false);
            mcrTextField.setVisible(false);
            mcrBtn.setEnabled(false);
            mcrBtn.setVisible(false);
            jLabel1.setText("Matlab Compiler Runtime is automatically set in Windows");
            jLabel1.setEnabled(false);
        }
        setLocationRelativeTo(this.getParent());
        super.setVisible(b);
    }
}
