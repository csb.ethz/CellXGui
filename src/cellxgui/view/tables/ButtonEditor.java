/*
 * ButtonEditor.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.tables;

import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ButtonEditor extends AbstractCellEditor implements TableCellEditor {

    protected final JButton button = CellXTableView.createTableButton();

    public ButtonEditor(String label) {
        button.setText(label);
        button.setBorderPainted(false);
        final Font f = button.getFont().deriveFont(Font.PLAIN);
        button.setFont(f.deriveFont(8));
        button.setMargin(new Insets(0, 0, 0, 0));
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return button;
    }

    public void addButtonActionListener(ActionListener al) {
        button.addActionListener(al);
    }
}
