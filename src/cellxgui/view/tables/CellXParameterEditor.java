/*
 * CellXParameterEditor.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.tables;

import cellxgui.controller.CellXParameter;
import cellxgui.controller.CellXParameter.Limit;
import java.awt.Component;
import java.io.File;
import java.text.NumberFormat;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXParameterEditor extends DefaultCellEditor {

    private final JTextField textField;
    private CellXParameter param;
    private Object oldValue, newValue;

    public CellXParameterEditor() {
        super(new JTextField());
        this.textField = (JTextField) getComponent();
    }

    @Override
    public Object getCellEditorValue() {
        if (newValue == null) {
            return oldValue;
        }
        return newValue;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        oldValue = value;
        newValue = null;
        if (value != null && param.isArray()) {
            final NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMaximumFractionDigits(2);
            textField.setText(arrayToString((Number[]) value, param, nf));
            return textField;
        }
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public boolean stopCellEditing() {
        try {
            if (oldValue == null && textField.getText().isEmpty()) {
                return super.stopCellEditing();
            }
            convertValue();
        } catch (Exception e) {
            Object[] options = {"Edit", "Revert"};
            int answer = JOptionPane.showOptionDialog(
                    SwingUtilities.getWindowAncestor(textField),
                    e.getMessage(),
                    "Invalid Text Entered",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[1]);
            if (answer == 1) { //Revert!
                return super.stopCellEditing();
            } else {
                return false;
            }
        }
        return super.stopCellEditing();
    }

    private void convertValue() throws Exception {
        newValue = null;
        if (param.isArray()) {

            String str = textField.getText().trim();
            if (str.length() > 0 && str.charAt(0) == '[') {
                str = str.substring(1);
            }
            if (str.length() > 0 && str.charAt(str.length() - 1) == ']') {
                str = str.substring(0, str.length() - 1);
            }
            str = str.trim();
            final String[] strNums = str.split("(;\\s*|,\\s*|\\s+)");
            Number[] tmp;
            if (param.isIntegerArray()) {
                tmp = new Integer[strNums.length];
                for (int i = 0; i < strNums.length; ++i) {
                    tmp[i] = convertToInteger(strNums[i]);
                    checkNumber(tmp[i], i);
                }
            } else {
                tmp = new Double[strNums.length];
                for (int i = 0; i < strNums.length; ++i) {
                    tmp[i] = convertToDouble(strNums[i]);
                    checkNumber(tmp[i], i);
                }
            }

            if (tmp != null) {
                Number elementNum = (Number) param.get(CellXParameter.PROPERTY_TYPE.ELEMENT_NUM);
                if (elementNum != null && elementNum.intValue() != tmp.length) {
                    throw new Exception("Expected number of elements is " + elementNum.intValue() + ", found " + tmp.length);
                }

                elementNum = (Number) param.get(CellXParameter.PROPERTY_TYPE.MIN_ELEMENT_NUM);
                if (elementNum != null && tmp.length < elementNum.intValue()) {
                    throw new Exception("Minimum number of elements is " + elementNum.intValue() + ", found " + tmp.length);
                }

                elementNum = (Number) param.get(CellXParameter.PROPERTY_TYPE.MAX_ELEMENT_NUM);
                if (elementNum != null && tmp.length < elementNum.intValue()) {
                    throw new Exception("Maximum number of elements is " + elementNum.intValue() + ", found " + tmp.length);
                }
            }
            newValue = tmp;

        } else if (param.isNumericValueClass()) {
            Number tmp;
            if (param.isInteger()) {
                tmp = convertToInteger(textField.getText());
            } else {
                tmp = convertToDouble(textField.getText());
            }
            checkNumber(tmp, -1);
            newValue = tmp;

        } else if (param.isFile()) {
            try {
                newValue = new File(textField.getText());
            } catch (NullPointerException e) {
                throw new Exception("Invalid file name");
            }
        } else {
            throw new Error("Unknown parameter type");
        }
    }

    public void setParam(CellXParameter p) {
        this.param = p;
    }

    //TODO check min max and fixed value set
    private Integer convertToInteger(String s) throws NumberFormatException {
        try {
            final Number d = Double.parseDouble(s);
            final double test = d.intValue();
            if (test == d.doubleValue()) {
                return d.intValue();
            } else {
                throw new NumberFormatException("Cannot convert '" + s + "' to integer!");
            }
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Cannot convert '" + s + "' to integer!");
        }
    }

    public Double convertToDouble(String s) throws NumberFormatException {
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Cannot convert '" + s + "' to integer!");
        }
    }

    public static String arrayToString(Number[] values, CellXParameter param, NumberFormat format) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < values.length; ++i) {
            if (param.isIntegerArray()) {
                sb.append(values[i].intValue());
            } else {
                if (format == null) {
                    sb.append(values[i]);
                } else {
                    sb.append(format.format(values[i]));
                }
            }
            if (i < values.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    private void checkNumber(Number n, int idx) throws Exception {
        if (param.has(CellXParameter.PROPERTY_TYPE.FIXED_VALUE_SET)) {
            final Number[] acceptedValues = (Number[]) param.get(CellXParameter.PROPERTY_TYPE.FIXED_VALUE_SET);

            for (Number an : acceptedValues) {
                if (an == n) {
                    return;
                }
            }
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < acceptedValues.length; ++i) {
                sb.append(acceptedValues[i]);
                if (i < acceptedValues.length - 1) {
                    sb.append(", ");
                }
            }

            throw new Exception("Possible values are{" + sb.toString() + "}");
        }


        if (param.has(CellXParameter.PROPERTY_TYPE.MIN)) {
            final Number minVal = (Number) param.get(CellXParameter.PROPERTY_TYPE.MIN);
            if (n.doubleValue() <= minVal.doubleValue()) {
                throw new Exception("Value must be > " + minVal);
            }
        }


        if (param.has(CellXParameter.PROPERTY_TYPE.MINEQ)) {
            final Number minVal = (Number) param.get(CellXParameter.PROPERTY_TYPE.MINEQ);
            if (n.doubleValue() < minVal.doubleValue()) {
                throw new Exception("Value must be >= " + minVal);
            }
        }


        if (param.has(CellXParameter.PROPERTY_TYPE.MAX)) {
            final Number maxVal = (Number) param.get(CellXParameter.PROPERTY_TYPE.MAX);
            if (n.doubleValue() >= maxVal.doubleValue()) {
                throw new Exception("Value must be < " + maxVal);
            }
        }

        if (param.has(CellXParameter.PROPERTY_TYPE.MAXEQ)) {
            final Number maxVal = (Number) param.get(CellXParameter.PROPERTY_TYPE.MAXEQ);
            if (n.doubleValue() > maxVal.doubleValue()) {
                throw new Exception("Value must be <= " + maxVal);
            }
        }

        if (idx >= 0) {
            final List<Limit> limits = param.getLimitsForIdx(idx);

            if (limits != null) {
                for (Limit l : limits) {
                    if (l.getType().equals(CellXParameter.PROPERTY_TYPE.MAXEQ)) {
                        if (n.doubleValue() > l.getLimit().doubleValue()) {
                            throw new Exception("Value at index " + String.valueOf(idx) + " must be <= " + l.getLimit());
                        }
                    }

                    if (l.getType().equals(CellXParameter.PROPERTY_TYPE.MINEQ)) {
                        if (n.doubleValue() < l.getLimit().doubleValue()) {
                            throw new Exception("Value at index " + String.valueOf(idx) + " must be >= " + l.getLimit());
                        }
                    }

                    if (l.getType().equals(CellXParameter.PROPERTY_TYPE.MAX)) {
                        if (n.doubleValue() > l.getLimit().doubleValue()) {
                            throw new Exception("Value at index " + String.valueOf(idx) + " must be < " + l.getLimit());
                        }
                    }

                    if (l.getType().equals(CellXParameter.PROPERTY_TYPE.MIN)) {
                        if (n.doubleValue() > l.getLimit().doubleValue()) {
                            throw new Exception("Value at index " + String.valueOf(idx) + " must be > " + l.getLimit());
                        }
                    }
                }
            }
        }
    }
}
