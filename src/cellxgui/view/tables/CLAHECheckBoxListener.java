/*
 * CLAHECheckBoxListener.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.tables;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CLAHECheckBoxListener implements CellEditorListener {

    private final CellXController control;
    private final CellXTableView tableView;

    public CLAHECheckBoxListener(CellXController control, CellXTableView tview) {
        this.control = control;
        this.tableView = tview;
    }

    @Override
    public void editingStopped(ChangeEvent e) {
        final int row = tableView.getSelectedRow();
        if (row < 0) {
            return;
        }
        if (row == CellXGuiConfiguration.getInstance().isGraphCutOnCLAHE.getLocation()) {
            control.updateCLAHEMode();
        } else if (row == CellXGuiConfiguration.getInstance().claheClipLimit.getLocation()) {
            control.updateCLAHEMode();
        } else if (row == CellXGuiConfiguration.getInstance().claheBlockSize.getLocation()) {
            control.updateCLAHEMode();
        }
    }

    @Override
    public void editingCanceled(ChangeEvent e) {
    }
}
