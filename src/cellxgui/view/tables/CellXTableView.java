/*
 * CellXTableView.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.tables;

import cellxgui.controller.CellXParameter;
import cellxgui.model.CellXGeneralParameterTableModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXTableView extends JTable {

    private final static int BUTTON_SIZE = 20;
    private CellXGeneralParameterTableModel cellXTableModel;
    private final CellXParameterEditor parameterEditor = new CellXParameterEditor();
    private final FileEditor fileEditor = new FileEditor();
    private final ButtonEditor autoBtnEditor = new ButtonEditor("A");
    private final ButtonEditor defaultBtnEditor = new ButtonEditor("D");
    private final ArrayRenderer arrayRenderer = new ArrayRenderer();
    private final ButtonRenderer defaultBtnRenderer = new ButtonRenderer("D");
    private final ButtonRenderer autoBtnRenderer = new ButtonRenderer("A");
    private final CheckBoxRenderer checkBoxRenderer = new CheckBoxRenderer();
    private final TableCellRenderer whiteRenderer = new WhiteRenderer();
    private final DefaultCellEditor checkBoxEditor = new DefaultCellEditor(new JCheckBox());
    private final ParamNameRenderer paramNameRenderer = new ParamNameRenderer();
    private AutoButtonHandler autoBtnHandler;

    public CellXTableView() {
        //this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        checkBoxEditor.setClickCountToStart(2);
        this.setRowHeight(BUTTON_SIZE + 2);
    }

    public void setAutoButtonHandler(AutoButtonHandler handler) {
        this.autoBtnHandler = handler;
        this.autoBtnEditor.addButtonActionListener(handler);
    }

    public void addCheckBoxListener(CellEditorListener cel) {
        checkBoxEditor.addCellEditorListener(cel);
    }

    @Override
    public void setModel(TableModel dataModel) {
        super.setModel(dataModel);
        if (dataModel instanceof CellXGeneralParameterTableModel) {
            this.cellXTableModel = (CellXGeneralParameterTableModel) dataModel;
            // fix the button column width 
            if (dataModel.getColumnCount() > 0) {
                getColumnModel().getColumn(0).setPreferredWidth(240);
                getColumnModel().getColumn(0).setMaxWidth(240);
                getColumnModel().getColumn(0).setMinWidth(240);
                getColumnModel().getColumn(2).setPreferredWidth(BUTTON_SIZE);
                getColumnModel().getColumn(2).setMinWidth(BUTTON_SIZE);
                getColumnModel().getColumn(2).setMaxWidth(BUTTON_SIZE);
                getColumnModel().getColumn(3).setPreferredWidth(BUTTON_SIZE);
                getColumnModel().getColumn(3).setMinWidth(BUTTON_SIZE);
                getColumnModel().getColumn(3).setMaxWidth(BUTTON_SIZE);
            }
        }
    }

    @Override
    public TableCellEditor getCellEditor(int row, int column) {
        if (cellXTableModel != null) {
            final CellXParameter p = cellXTableModel.getParameterByRowIndex(row);
            if (column == 1) {
                if (p.isFile()) {
                    fileEditor.setParam(p);
                    return fileEditor;
                } else if (p.isBoolean()) {
                    return checkBoxEditor;
                } else {
                    parameterEditor.setParam(p);
                    return parameterEditor;
                }
            } else if (column == 2) {
                if (autoBtnHandler != null && autoBtnHandler.hasAutoMethod(p)) {
                    return autoBtnEditor;
                }
            } else if (column == 3) {
                return defaultBtnEditor;
            }
        }
        return null;

    }

    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        if (cellXTableModel != null) {
            final CellXParameter p = cellXTableModel.getParameterByRowIndex(row);
            if (column == 0) {
                paramNameRenderer.setParam(p);
                return paramNameRenderer;
            } else if (column == 1) {
                if (p.isArray()) {
                    arrayRenderer.setParam(p);
                    return arrayRenderer;
                } else if (p.isBoolean()) {
                    return checkBoxRenderer;
                }
            } else if (column == 2) {
                if (autoBtnHandler != null && autoBtnHandler.hasAutoMethod(p)) {
                    return autoBtnRenderer;
                } else {
                    return whiteRenderer;
                }
            } else if (column == 3) {
                return defaultBtnRenderer;
            }
        }
        return super.getCellRenderer(row, column);
    }

    public void addActionListenerToDefaultButton(ActionListener a) {
        defaultBtnEditor.addButtonActionListener(a);
    }

    class ArrayRenderer extends DefaultTableCellRenderer {

        private CellXParameter p;

        public ArrayRenderer() {
            super();
        }

        public void setParam(CellXParameter p) {
            this.p = p;
        }

        @Override
        public void setValue(Object value) {
            if (value != null) {
                final NumberFormat nf = NumberFormat.getNumberInstance();
                nf.setMaximumFractionDigits(2);
                setText(CellXParameterEditor.arrayToString((Number[]) value, p, nf));
            } else {
                setText("");
            }
        }
    }

    class ButtonRenderer extends JPanel implements TableCellRenderer {

        private final JButton button = createTableButton();

        public ButtonRenderer(String label) {
            button.setText(label);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            return button;
        }
    }

    class CheckBoxRenderer implements TableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            final TableCellRenderer tcr = table.getDefaultRenderer(Boolean.class);
            final Component c = tcr.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (c instanceof JCheckBox) {
                final JCheckBox cb = (JCheckBox) c;
                cb.setOpaque(true);
                cb.setHorizontalAlignment(SwingConstants.LEFT);
            }
            return c;
        }
    }

    class WhiteRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    };

    class ParamNameRenderer extends DefaultTableCellRenderer {

        private CellXParameter param;

        public void setParam(CellXParameter param) {
            this.param = param;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (!param.getToolTipText().isEmpty()) {
                setToolTipText(param.getToolTipText());
            }
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

    public static JButton createTableButton() {
        final JButton ret = new JButton();
        ret.setBorderPainted(false);
        ret.setFont(ret.getFont().deriveFont(Font.PLAIN, 10.0f));
        ret.setMargin(new Insets(0, 0, 0, 0));
        ret.setForeground(Color.darkGray);
        UIDefaults def = new UIDefaults();
        if (def != null) {
            def.put("Button.contentMargins", new Insets(0, 0, 0, 0));
            ret.putClientProperty("Nimbus.Overrides", def);
        }
        return ret;
    }
}
