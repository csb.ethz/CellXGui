/*
 * ShapeOverlay.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import cellxgui.model.shapes.SelectableShape;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ShapeOverlay implements PaintableOverlay {

    private final ZoomImagePanel zip;
    private BufferedImage overlayImage;
    private List<? extends SelectableShape> shapeList;

    public ShapeOverlay(ZoomImagePanel zip) {
        this.zip = zip;
    }

    public void setOverlay(BufferedImage img) {
        this.overlayImage = img;
    }

    public void setOverlayShapeList(List<? extends SelectableShape> shapes) {
        this.shapeList = shapes;
    }

    @Override
    public void paint(Graphics2D g2) {
        if (shapeList != null) {
            for (SelectableShape s : shapeList) {
                s.paint(g2);
            }
        }
    }
}
