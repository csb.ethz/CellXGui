/*
 * Circle.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class Circle extends Line {

    public final double diameter;
    public final double cx, cy;
    private final Ellipse2D.Double ellipse;

    public Circle(double cx, double cy, double diameter) {
        super((int) Math.round(cx), (int) Math.round(cy), (int) Math.round(Math.round(cx) + diameter), (int) Math.round(Math.round(cy) + diameter));
        this.diameter = diameter;
        this.cx = cx;
        this.cy = cy;
        this.ellipse = new Ellipse2D.Double(cx, cy, diameter, diameter);
    }

    @Override
    public void paint(Graphics2D g2) {
        g2.draw(ellipse);
    }
}
