/*
 * ExtendedZoomImagePanel.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.controller.CellXParameter.PROPERTY_TYPE;
import cellxgui.model.shapes.Coordinates;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ExtendedZoomImagePanel extends ZoomImagePanel {

    private final CellXController controller;
    protected final ShapeOverlay shapeOverlay;
    protected PaintableCoordinates currentShape;
    protected Mode currentMode = Mode.ZOOM_PAN;
    private boolean antiAliasing;
    private final int MIN_MEMBRANE_PROFILE_LENGTH;
    private final int MIN_SEED_DIAMETER;
    private final int MIN_CELL_LENGTH;

    public ExtendedZoomImagePanel(CellXController controller) {
        super();
        setDefaultMouseMotionListenerEnabled(false);
        setDefaultMouseListerenEnabled(false);

        shapeOverlay = new ShapeOverlay(this);
        this.controller = controller;
        final MouseManager mm = new MouseManager();
        this.addMouseListener(mm);
        this.addMouseMotionListener(mm);

        this.MIN_CELL_LENGTH = ((Number) CellXGuiConfiguration.getInstance().maximumCellLengthParameter.get(PROPERTY_TYPE.MINEQ)).intValue();
        this.MIN_SEED_DIAMETER = 2 * ((Number) CellXGuiConfiguration.getInstance().seedRadiusLimitParameter.get(PROPERTY_TYPE.MINEQ)).intValue();
        this.MIN_MEMBRANE_PROFILE_LENGTH = ((Number) CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.get(PROPERTY_TYPE.MIN_ELEMENT_NUM)).intValue();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        final Graphics2D g2 = (Graphics2D) g;
        //System.out.printf("Display image mag ratio %f \n", getDisplayToImageRatio());
        if (getDisplayToImageRatio() < 1.0) {
            //System.out.println("Enabled bicubic interpolation");
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        }

        if (antiAliasing) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }


        final Rectangle r = getCurrentSourceRectangle();
        final AffineTransform current = g2.getTransform();
        // do not change the order of transformations
        g2.transform(AffineTransform.getScaleInstance(getWidth() / (double) r.width, getHeight() / (double) r.height));
        g2.translate(-r.x + 0.5, -r.y + 0.5);

        shapeOverlay.paint(g2);
        g2.setTransform(current);

        if (currentShape != null) {
            g2.setColor(currentMode.color);
            currentShape.paint(g2);
        }
    }

    public ShapeOverlay getShapeOverlay() {
        return shapeOverlay;
    }

    public void setMode(Mode mode) {
        currentMode = mode;
        currentShape = null;
        setMousePointer(mode);
    }

    private void setMousePointer(Mode mode) {
        if (mode.equals(Mode.ZOOM_PAN)) {
            Cursor c = new Cursor(Cursor.HAND_CURSOR);
            this.setCursor(c);
        } else {
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void setAntiAliasingEnabled(boolean b) {
        antiAliasing = b;
    }

    class MouseManager implements MouseMotionListener, MouseListener {

        private Point oldMouseLoc;
        private Point mouseDownPoint;

        @Override
        public void mouseDragged(MouseEvent e) {
            if (currentMode.equals(Mode.ZOOM_PAN)) {
                dragCenter(oldMouseLoc.x, oldMouseLoc.y, e.getX(), e.getY());
            } else if (currentMode.equals(Mode.MEMBRANE_PROFILE)) {
                if (e.isControlDown() || e.isMetaDown()) {
                    if (e.isShiftDown() || e.isAltDown()) {
                        currentShape = new Star(mouseDownPoint.x, mouseDownPoint.y, e.getX(), e.getY(), 12);
                    } else {
                        currentShape = new Star(mouseDownPoint.x, mouseDownPoint.y, e.getX(), e.getY(), 3);
                    }
                } else {
                    currentShape = new LineWithArrow(mouseDownPoint.x, mouseDownPoint.y, e.getX(), e.getY());
                }
            } else if (currentMode.equals(Mode.SEED_SIZE)) {
                final double dx = e.getX() - mouseDownPoint.getX();
                final double dy = e.getY() - mouseDownPoint.getY();
                final double radius = Math.sqrt(dx * dx + dy * dy);
                currentShape = new Circle(e.getX() - radius, e.getY() - radius, 2 * radius);
            } else if (currentMode.equals(Mode.CELL_LENGTH)) {
                currentShape = new Line(mouseDownPoint.x, mouseDownPoint.y, e.getX(), e.getY());
            } else if (currentMode.equals(Mode.CROP)) {
                currentShape = new CropRectangle(mouseDownPoint.x, mouseDownPoint.y, e.getX(), e.getY());
            }
            repaint();
            oldMouseLoc = e.getPoint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (currentMode.equals(Mode.ZOOM_PAN)) {
                setCenter(e.getX(), e.getY());
                if (e.getButton() == MouseEvent.BUTTON1 && !(e.isControlDown())) {
                    increaseMagnification();
                } else {
                    decreaseMagnification();
                }
            }
            repaint();
            oldMouseLoc = e.getPoint();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            oldMouseLoc = e.getPoint();
            mouseDownPoint = e.getPoint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.getX() < 0 || e.getX() > getWidth()) {
                currentShape = null;
                repaint();
                return;
            }
            if (e.getY() < 0 || e.getY() > getHeight()) {
                currentShape = null;
                repaint();
                return;
            }

            if (currentMode.equals(Mode.MEMBRANE_PROFILE)) {
                if (currentShape instanceof Star) {
                    final Star s = (Star) currentShape;
                    for (Line l : s.getLines()) {
                        final Coordinates coords = tranformToImageCoordinates(l);
                        if (coords.getNumberOfPixelsOnConnectingLine() >= MIN_MEMBRANE_PROFILE_LENGTH) {
                            controller.addMembraneProfile(coords);
                        }
                    }
                } else if (currentShape != null) {
                    final Coordinates coords = tranformToImageCoordinates(currentShape);
                    if (coords.getNumberOfPixelsOnConnectingLine() >= MIN_MEMBRANE_PROFILE_LENGTH) {
                        controller.addMembraneProfile(coords);
                    }
                }
            } else if (currentMode.equals(Mode.SEED_SIZE)) {
                if (currentShape != null) {
                    final Coordinates coords = tranformToImageCoordinates(currentShape);
                    if (coords.getNumberOfPixelsOnConnectingLine() >= MIN_SEED_DIAMETER) {
                        controller.addSeedSize(coords);
                    }
                }
            } else if (currentMode.equals(Mode.CELL_LENGTH)) {
                if (currentShape != null) {
                    final Coordinates coords = tranformToImageCoordinates(currentShape);
                    if (coords.getNumberOfPixelsOnConnectingLine() >= MIN_CELL_LENGTH) {
                        controller.addCellLength(coords);
                    }
                }
            } else if (currentMode.equals(Mode.CROP)) {
                if (currentShape != null) {
                    final Coordinates coords = tranformToImageCoordinates(currentShape);
                    if (coords.getNumberOfPixelsOnConnectingLine() >= CellXGuiConfiguration.MIN_CROP_WIDTH) {
                        controller.setCropRegion(coords);
                    }
                }
            }
            currentShape = null;
            repaint();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

    private Coordinates tranformToImageCoordinates(PaintableCoordinates p) {


        final Coordinates ret = new Coordinates(
                canvasX2image(p.getX1()),
                canvasY2image(p.getY1()),
                canvasX2image(p.getX2()),
                canvasY2image(p.getY2()));
        return ret;
    }
}
