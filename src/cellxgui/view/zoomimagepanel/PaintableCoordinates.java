/*
 * PaintableCoordinates.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Graphics2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public interface PaintableCoordinates {

    public void paint(Graphics2D g2);

    public int getX1();

    public int getX2();

    public int getY1();

    public int getY2();
}
