/*
 * Star.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class Star extends Line {

    protected final int numRays;
    protected final double theta;
    protected final ArrayList<LineWithArrow> profiles;

    public Star(int x1, int y1, int x2, int y2, int numRays) {
        super(x1, y1, x2, y2);
        this.numRays = numRays;
        this.theta = 2 * Math.PI / numRays;
        this.profiles = generateProfiles();
    }

    @Override
    public void paint(Graphics2D g2) {
        for (LineWithArrow lwa : profiles) {
            lwa.paint(g2);
        }
    }

    public Iterable<? extends Line> getLines() {
        return profiles;
    }

    private ArrayList<LineWithArrow> generateProfiles() {
        final ArrayList<LineWithArrow> ret = new ArrayList<LineWithArrow>(numRays);
        final AffineTransform a = AffineTransform.getRotateInstance(theta);
        Point2D src = new Point2D.Double(x2 - x1, y2 - y1);
        for (int i = 0; i < numRays; ++i) {
            ret.add(new LineWithArrow(x1, y1, (int) (x1 + src.getX()), (int) (y1 + src.getY())));
            src = a.transform(src, null);
        }
        return ret;
    }
}
