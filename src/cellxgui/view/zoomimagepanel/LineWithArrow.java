/*
 * LineWithArrow.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import cellxgui.model.shapes.MembraneProfile;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class LineWithArrow extends Line {

    private final Shape tip = MembraneProfile.generateTip();

    public LineWithArrow(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }

    @Override
    public void paint(Graphics2D g2) {
        super.paint(g2);
        final AffineTransform a = g2.getTransform();
        g2.translate(x2, y2);
        double angle = Math.PI / 2.0;
        if ((y1 < y2)) {
            angle = Math.PI + Math.atan((x2 - x1) / (double) (y1 - y2));
        } else if (y2 < y1) {
            angle = Math.atan((x2 - x1) / (double) (y1 - y2));
        } else if (x1 > x2) {
            angle = -Math.PI / 2.0;
        }
        g2.rotate(angle);
        g2.draw(tip);
        g2.setTransform(a);
    }
}
