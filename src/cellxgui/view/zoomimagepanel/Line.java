/*
 * Line.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class Line implements PaintableCoordinates {

    public final int x1, y1, x2, y2;
    protected final Line2D.Double line;

    public Line(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        this.line = new Line2D.Double(x1, y1, x2, y2);
    }

    @Override
    public void paint(Graphics2D g2) {
        g2.draw(line);
    }

    @Override
    public int getX1() {
        return x1;
    }

    @Override
    public int getX2() {
        return x2;
    }

    @Override
    public int getY1() {
        return y1;
    }

    @Override
    public int getY2() {
        return y2;
    }
}
