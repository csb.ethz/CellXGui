/*
 * TimeSeriesXMLWriter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.filehandling.CellXFileSet;
import cellxgui.controller.filehandling.CellXFluoFile;
import cellxgui.controller.filehandling.CellXTimeSeries;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class TimeSeriesXMLWriter {

    private boolean wasSuccesful;
    private Document doc;
    private Element root;

    public void writeAllSeriesToFile(File file, List<CellXTimeSeries> series) {
        wasSuccesful = false;
        try {
            createDocument();
            for (CellXTimeSeries fs : series) {
                final Element ts = buildTimeSeriesXmlTreeFor(fs, doc);
                root.appendChild(ts);
            }

            final Source source = new DOMSource(doc);
            final Result result = new StreamResult(file);
            final Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty(OutputKeys.STANDALONE, "no");
            t.transform(source, result);
            wasSuccesful = true;
        } catch (TransformerConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (TransformerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (ParserConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }
    }

    public void writeOneSeriesToFile(File file, CellXTimeSeries ts) {
        wasSuccesful = false;
        try {
            createDocument();
            final Element tsNode = buildTimeSeriesXmlTreeFor(ts, doc);
            root.appendChild(tsNode);

            final Source source = new DOMSource(doc);
            final Result result = new StreamResult(file);
            final Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty(OutputKeys.STANDALONE, "no");
            t.transform(source, result);
            wasSuccesful = true;
        } catch (TransformerConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (TransformerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (ParserConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }
    }

    public void writeSelectedSeriesToFile(File file, List<CellXTimeSeries> series, int[] selected) {
        wasSuccesful = false;
        try {
            createDocument();
            for (int i = 0; i < selected.length; ++i) {
                final Element ts = buildTimeSeriesXmlTreeFor(series.get(selected[i]), doc);
                root.appendChild(ts);
            }
            final Source source = new DOMSource(doc);
            final Result result = new StreamResult(file);
            final Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty(OutputKeys.STANDALONE, "no");
            t.transform(source, result);
            wasSuccesful = true;
        } catch (TransformerConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (TransformerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (ParserConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }
    }

    private void createDocument() throws ParserConfigurationException {
        this.doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        this.root = doc.createElement(CellXGuiConfiguration.CellXFilesXMLRootNodeName);
        final DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        root.setAttribute("timestamp", dateFormat.format(new Date()));
        root.setAttribute("creator", "CellXGui");
        doc.appendChild(root);
    }

    public boolean wasSuccessful() {
        return wasSuccesful;
    }

    private Element buildTimeSeriesXmlTreeFor(CellXTimeSeries ts, Document doc) {
        final Element fileSeriesElement = doc.createElement(CellXGuiConfiguration.CellXTimeSeriesXMLNodeName);
        fileSeriesElement.setAttribute(CellXGuiConfiguration.CellXTimeSeriesXMLIdAttr, String.valueOf(ts.getId()));
        fileSeriesElement.setAttribute(CellXGuiConfiguration.CellXTimeSeriesXMLPositionAttr, String.valueOf(ts.getPosition()));
        fileSeriesElement.setAttribute(CellXGuiConfiguration.CellXTimeSeriesXMLTrackingAttr, String.valueOf(ts.isTrackingEnabled() ? "1" : "0"));
        fileSeriesElement.setAttribute(CellXGuiConfiguration.CellXTimeSeriesXMLFluoTypesAttr, ts.getFluoTypesString());

        //set result dir
        final Element resultDirElement = doc.createElement(CellXGuiConfiguration.CellXResultDirXMLNodeName);
        resultDirElement.setTextContent(ts.getResultDirectory().toString());
        fileSeriesElement.appendChild(resultDirElement);

        //append file sets       
        for (CellXFileSet fs : ts.getFileSets()) {
            final Element fsNode = buildFileSetXmlTreeFor(fs, doc);
            fileSeriesElement.appendChild(fsNode);
        }
        return fileSeriesElement;
    }

    private Element buildFileSetXmlTreeFor(CellXFileSet fs, Document doc) {
        final Element fsNode = doc.createElement(CellXGuiConfiguration.CellXFileSetXMLNodeName);
        fsNode.setAttribute(CellXGuiConfiguration.CellXFileSetFrameAttrName, String.valueOf(fs.getFrameIdx()));
        final Element oofNode = doc.createElement(CellXGuiConfiguration.CellXOofImageXMLNodeName);
        fsNode.appendChild(oofNode);
        oofNode.setTextContent(fs.getOutOfFocusImage().toString());
        for (CellXFluoFile fluoObj : fs.getFluoFiles()) {
            final Element fluoNode = doc.createElement(CellXGuiConfiguration.CellXFluoSetXMLNodeName);
            fsNode.appendChild(fluoNode);
            fluoNode.setAttribute(CellXGuiConfiguration.CellXFluoTypeAttrName, fluoObj.getType());
            final Element fluoImgNode = doc.createElement(CellXGuiConfiguration.CellXFluoImageXMLNodeName);
            fluoNode.appendChild(fluoImgNode);
            fluoImgNode.setTextContent(fluoObj.getFluoImage().toString());
            if (fluoObj.hasFlatFieldImage()) {
                final Element ffNode = doc.createElement(CellXGuiConfiguration.CellXFfImageXMLNodeName);
                ffNode.setTextContent(fluoObj.getFlatFieldImage().toString());
                fluoNode.appendChild(ffNode);
            }
        }
        return fsNode;
    }
}
