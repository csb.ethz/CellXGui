package cellxgui.model.shapes;

import cellxgui.view.zoomimagepanel.PaintableOverlay;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellLength extends SelectableShape implements PaintableOverlay {

    public CellLength(Coordinates coords, Color color, Color selectionColor) {
        super(coords, color, selectionColor);
    }

    @Override
    public void paint(Graphics2D g2) {
        g2.setColor(drawColor);
        g2.drawLine(coords.x1, coords.y1, coords.x2, coords.y2);
        paintBoundingBox(g2);
    }

    @Override
    public String toString() {
        return String.format("CellLength(%dpx)", coords.getNumberOfPixelsOnConnectingLine());
    }

    public int getPixelLength() {
        return coords.getNumberOfPixelsOnConnectingLine();
    }
}
