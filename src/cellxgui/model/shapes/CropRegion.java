package cellxgui.model.shapes;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CropRegion extends SelectableShape{

    public CropRegion(Coordinates coords, Color color, Color selectionColor) {
        super(coords, color, selectionColor);
    }

    @Override
    public void paint(Graphics2D g2) {
        g2.setColor(drawColor);
        g2.drawRect(coords.getLX(), coords.getLY(), coords.getWidth(), coords.getHeight());
        paintBoundingBox(g2);
    }

    @Override
    public String toString() {
        return String.valueOf("Crop");
    }
    
    
    public Integer[] getCropRegion(){
        Integer[] ret = new Integer[4];
        ret[0] = coords.getLX();
        ret[1] = coords.getLY();
        ret[2] = coords.getWidth();
        ret[3] = coords.getHeight();
        return ret;
    }
}
