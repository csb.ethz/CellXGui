/*
 * SelectableShape.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.Serializable;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public abstract class SelectableShape implements Serializable {

    private static final long serialVersionUID = 1L;
    protected boolean selected = false;
    protected final Coordinates coords;
    protected final Color selectColor;
    protected final Color drawColor;

    public SelectableShape(Coordinates coords, Color color, Color selectColor) {
        this.coords = coords;
        this.drawColor = color;
        this.selectColor = selectColor;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean b) {
        selected = b;
    }

    public Coordinates getCoordinates() {
        return coords;
    }

    public abstract void paint(Graphics2D g2);

    public void paintBoundingBox(Graphics2D g2) {
        if (selected) {
            g2.setColor(selectColor);
            g2.drawRect(coords.getBoundingBoxX(), coords.getBoundingBoxY(), coords.getBoundinBoxWidth(), coords.getBoundingBoxHeight());
        }
    }
}
