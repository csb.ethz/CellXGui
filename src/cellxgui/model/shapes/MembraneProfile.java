/*
 * MembraneProfile.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.shapes;

import cellxgui.model.signalaligner.Signal;
import ij.process.ImageProcessor;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembraneProfile extends SelectableShape implements Signal {

    private double mean;
    private double[] values;
    
    private final Shape tip = generateTip();

    public MembraneProfile(Coordinates coords, Color color, Color selectColor, ImageProcessor ip) {
        super(coords, color, selectColor);
        computeProfile(coords.x1, coords.y1, coords.x2, coords.y2, ip);
    }

    public final void computeProfile(int x0, int y0, int x1, int y1, ImageProcessor ip) {
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);

        values = new double[Math.max(dx, dy)];

        int err = dx - dy;

        int sx, sy;
        if (x0 < x1) {
            sx = 1;
        } else {
            sx = -1;
        }

        if (y0 < y1) {
            sy = 1;
        } else {
            sy = -1;
        }

        int i = 0, e2 = 0;

        final double range = ip.getMax() - ip.getMin();
        final double minVal = ip.getMin();
        while (i < values.length) {
            final double val = (ip.getPixelValue(x0, y0) - minVal) / range;
            values[i] = val;
            mean += val;
            ++i;
            if (x0 == x1 && y0 == y1) {
                break;
            }
            e2 = 2 * err;
            if (e2 > -dy) {
                err = err - dy;
                x0 = x0 + sx;
            }
            if (e2 < dx) {
                err = err + dx;
                y0 = y0 + sy;
            }
        }
        mean /= i;
        for (i = 0; i < values.length; ++i) {
            values[i] -= mean;
        }

    }

    @Override
    public void paint(Graphics2D g2) {
        g2.setColor(drawColor);
        g2.drawLine(coords.x1, coords.y1, coords.x2, coords.y2);
        final AffineTransform a = g2.getTransform();
        g2.translate(coords.x2, coords.y2);
        double angle = Math.PI / 2.0;
        if ((coords.y1 < coords.y2)) {
                angle = Math.PI +Math.atan((coords.x2 - coords.x1) / (double) (coords.y1 - coords.y2));
        } else if (coords.y2 < coords.y1) {
                angle = Math.atan((coords.x2 - coords.x1) / (double) (coords.y1 - coords.y2));
        }else if( coords.x1>coords.x2){
            angle = -Math.PI / 2.0;
        }
        g2.rotate(angle);
        g2.draw(tip);
        g2.setTransform(a);
        paintBoundingBox(g2);
    }

    @Override
    public String toString() {
        return String.format("MProfile(%dpx)", coords.getNumberOfPixelsOnConnectingLine());
    }

    @Override
    public double[] getNormalizedValues() {
        return values;
    }

    @Override
    public int getLength() {
        return values.length;
    }

    public static Shape generateTip() {
        GeneralPath gp = new GeneralPath();    
        gp.moveTo(-3, 6);
        gp.lineTo(0, -1);
        gp.lineTo(3, 6);
        return gp;
    }
    
    public double[] getMinMaxValue(){      
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for(int i=0; i<values.length; ++i){
            if(values[i]< min){
                min = values[i];
            }
            if( values[i]>max){
                max = values[i];
            }
        }
        return new double[]{min,max};       
    }
    
    
    public double getValue(int i){
        return values[i];
    }
    
}
