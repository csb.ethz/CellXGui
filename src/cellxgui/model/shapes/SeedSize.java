package cellxgui.model.shapes;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class SeedSize extends SelectableShape {

    public SeedSize(Coordinates coords, Color color, Color selectColor) {
        super(coords, color, selectColor);
    }

    @Override
    public void paint(Graphics2D g2) {
        g2.setColor(drawColor);
        g2.drawOval(coords.x1, coords.y1, coords.getWidth(), coords.getWidth());
        paintBoundingBox(g2);
    }

    @Override
    public String toString() {
        return String.format("SeedSize(%dpx)", coords.getNumberOfPixelsOnConnectingLine() / 2);
    }

    public int getPixelRadius() {
        return coords.getNumberOfPixelsOnConnectingLine() / 2;
    }
}
