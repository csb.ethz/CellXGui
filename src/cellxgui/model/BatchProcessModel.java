/*
 * BatchProcessModel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiProperties;
import cellxgui.controller.filehandling.CellXTimeSeries;
import cellxgui.view.dialogs.wizard.NoFluoPatternsDefined;
import cellxgui.view.dialogs.wizard.NoGroupInRegexpException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class BatchProcessModel extends AbstractModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Pattern oofPattern;
    private List<Pattern> fluoPatterns = new ArrayList<Pattern>();
    private boolean doCellTracking = true;
    private Pattern frameIdxRegexp;
    private Pattern flatFieldRegexp;
    private File imageDir;
    private File resultsDirectory;
    private CellXController control;
    private List<CellXTimeSeries> timeSeriesList;

    public BatchProcessModel(CellXController control) {
        setController(control);
    }

    public BatchProcessModel() {
    }

    public void fireAllProperties() {
        fireOutOFFocusPattern();
        fireFluoPatterns();
        firePropertyChange(CellXController.DO_CELL_TRACKING, null, doCellTracking);
        fireFrameIdxPattern();
        fireFlatFieldPattern();
        fireImageDir();
        fireBatchResultsDir();
        firePropertyChange(CellXController.FILE_SET_PREVIEW, null, new ArrayList<String>());
    }

    public void fireOutOFFocusPattern() {
        firePropertyChange(CellXController.OOF_PATTERN, null, oofPattern == null ? "" : oofPattern.toString());
    }

    private void fireFluoPatterns() {
        firePropertyChange(CellXController.FLUO_PATTERNS, null, fluoPatterns);
    }

    public void fireFrameIdxPattern() {
        firePropertyChange(CellXController.FRAME_IDX_PATTERN, null, frameIdxRegexp == null ? "" : frameIdxRegexp.toString());
    }

    public void fireFlatFieldPattern() {
        firePropertyChange(CellXController.FLAT_FIELD_PATTERN, null, flatFieldRegexp == null ? "" : flatFieldRegexp.toString());
    }

    public void fireImageDir() {
        firePropertyChange(CellXController.BATCH_IMAGE_DIR, null, imageDir == null ? "" : imageDir.toString());
    }

    public void fireBatchResultsDir() {
        firePropertyChange(CellXController.BATCH_RESULTS_DIR, null, resultsDirectory == null ? "" : resultsDirectory.toString());
    }


    /*
     * GET
     */
    public Pattern getFlatFieldPattern() {
        return flatFieldRegexp;
    }

    public File getImageDir() {
        return imageDir;
    }

    public Pattern getFramePattern() {
        return frameIdxRegexp;
    }

    public Pattern getOOFPattern() {
        return oofPattern;
    }

    public List<Pattern> getFluoPatterns() {
        return fluoPatterns;
    }

    public File getBatchResultsDir() {
        return resultsDirectory;
    }

    public CellXTimeSeries getFileSeries(int selectedIdx) {
        if (timeSeriesList == null) {
            return null;
        }
        return timeSeriesList.get(selectedIdx);
    }

    public List<CellXTimeSeries> getTimeSeriesList() {
        return timeSeriesList;
    }

    public String getFluorescenceGroup() throws NoGroupInRegexpException, NoFluoPatternsDefined {
        //match the first parentheses that are not escaped

        if (fluoPatterns.isEmpty()) {
            throw new NoFluoPatternsDefined();
        }

        final Pattern matchFirstGroupBehindFirstCharacter = Pattern.compile(".*?[^\\\\]\\((.+?[^\\\\])\\).*");
        final Pattern matchGoupAtStringBegin = Pattern.compile("^\\((.+?[^\\\\])\\).*");
        final StringBuilder sb = new StringBuilder();
        sb.append("(");
        boolean first = true;
        for (Pattern p : fluoPatterns) {
            Matcher m = matchFirstGroupBehindFirstCharacter.matcher(p.toString());
            if (m.matches()) {
                if (!first) {
                    sb.append("|");
                }
                first = false;
                sb.append(m.group(1));
            } else {
                m = matchGoupAtStringBegin.matcher(p.toString());
                if (m.matches()) {
                    if (!first) {
                        sb.append("|");
                    }
                    first = false;
                    sb.append(m.group(1));
                } else {
                    throw new NoGroupInRegexpException(p.toString());
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public boolean isTrackingEnabled() {
        return doCellTracking;
    }

    /*
     * SET
     */
    public final void setController(CellXController control) {
        this.control = control;
        this.addPropertyChangeListener(control);
    }

    public void setFrameIdxPattern(Pattern p) {
        firePropertyChange(CellXController.FRAME_IDX_PATTERN, frameIdxRegexp == null ? "" : frameIdxRegexp.toString(), p == null ? "" : p.toString());
        this.frameIdxRegexp = p;
    }

    public void setOutOfFocusPattern(Pattern p) {
        firePropertyChange(CellXController.OOF_PATTERN, oofPattern == null ? "" : oofPattern.toString(), p == null ? "" : p.toString());
        this.oofPattern = p;
    }

    public void setTrackingEnabled(boolean selected) {
        firePropertyChange(CellXController.DO_CELL_TRACKING, doCellTracking, selected);
        doCellTracking = selected;
    }

    public void setBatchImageDirectory(File selectedFile) {
        firePropertyChange(CellXController.BATCH_IMAGE_DIR, imageDir == null ? "" : imageDir.toString(), selectedFile.toString());
        this.imageDir = selectedFile;
    }

    public void setFileSetSeries(List<CellXTimeSeries> timeSeriesList) {
        this.timeSeriesList = timeSeriesList;
        firePropertyChange(CellXController.FILE_SET_PREVIEW, null, timeSeriesList);
    }

    public void setFlatFieldPattern(Pattern p) {
        firePropertyChange(CellXController.FLAT_FIELD_PATTERN, flatFieldRegexp == null ? "" : flatFieldRegexp.toString(), p == null ? "" : p.toString());
        this.flatFieldRegexp = p;
    }

    public void setBatchResultsDirectory(File selectedFile) {
        firePropertyChange(CellXController.BATCH_RESULTS_DIR, resultsDirectory == null ? "" : resultsDirectory.toString(), selectedFile == null ? "" : selectedFile.toString());
        this.resultsDirectory = selectedFile;
    }


    /*
     *
     */
    public void addFluoTypePattern(Pattern p) {
        fluoPatterns.add(p);
        firePropertyChange(CellXController.FLUO_PATTERNS, null, fluoPatterns);
    }

    public void deleteFluoPatterns(int[] selectedIndices) {
        for (int i = selectedIndices.length - 1; i >= 0; --i) {
            fluoPatterns.remove(selectedIndices[i]);
        }
        fireFluoPatterns();
    }

    public void resetFileSeries() {
        this.timeSeriesList = null;
        firePropertyChange(CellXController.FILE_SET_PREVIEW, null, new ArrayList<String>());
    }

    public void initFromPropertiesFile(CellXGuiProperties properties) {
        try {
            if (properties.containsOOFPattern()) {
                this.oofPattern = properties.getOOFPattern();
            }
            if (properties.containsFluoPatterns()) {
                for (String s : properties.getFluoPatterns()) {
                    this.fluoPatterns.add(Pattern.compile(s));
                }
            }
            if (properties.containsFramePattern()) {
                this.frameIdxRegexp = properties.getFramePattern();
            }

            if (properties.containsFlatFieldPattern()) {
                this.flatFieldRegexp = properties.getFlatFieldPattern();
            }

            if (properties.containsTrackingEnabled()) {
                this.doCellTracking = properties.isTrackingEnabled();
            }

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(oofPattern);
        out.writeObject(frameIdxRegexp);
        out.writeObject(flatFieldRegexp);
        out.writeObject(fluoPatterns);
        out.writeBoolean(doCellTracking);
        out.writeObject(imageDir);
        out.writeObject(resultsDirectory);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        oofPattern = (Pattern) in.readObject();
        frameIdxRegexp = (Pattern) in.readObject();
        flatFieldRegexp = (Pattern) in.readObject();
        fluoPatterns = (List<Pattern>) in.readObject();
        doCellTracking = in.readBoolean();
        imageDir = (File) in.readObject();
        resultsDirectory = (File) in.readObject();
    }
}
