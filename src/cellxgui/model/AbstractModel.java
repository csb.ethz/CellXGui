/*
 * AbstractModel.java
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * From: http://www.oracle.com/technetwork/articles/javase/mvc-136693.html
 */
public abstract class AbstractModel {

    final protected PropertyChangeSupport propertyChangeSupport;

    public AbstractModel() {
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void removeAllPropertyChangeListener() {
        for (PropertyChangeListener pcl : propertyChangeSupport.getPropertyChangeListeners()) {
            propertyChangeSupport.removePropertyChangeListener(pcl);
        }
    }
}
