/*
 * MembraneProfileMode.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.modes;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.model.shapes.MembraneProfile;
import cellxgui.model.signalaligner.XYDatasetSignalAligner;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembraneProfileMode extends AbstractMode<MembraneProfile> {

    private final Mode mode;
    private final XYDatasetSignalAligner signalAligner = new XYDatasetSignalAligner(CellXGuiConfiguration.percentageThrCoverage);

    public MembraneProfileMode(Mode mode) {
        super();
        this.mode = mode;
    }

    @Override
    public void addShape(MembraneProfile s) {
        this.shapes.add(s);
        this.signalAligner.addSignal(s);
    }

    @Override
    public void deleteSelectedShapes(int[] indices) {
        super.deleteSelectedShapes(indices);
        signalAligner.reset();
        signalAligner.addAllSignals(this.shapes);
    }

    public XYDatasetSignalAligner getSignalAligner() {
        return signalAligner;
    }
}
