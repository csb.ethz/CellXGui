/*
 * ZoomMode.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.modes;

import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.model.shapes.SelectableShape;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ZoomMode extends AbstractMode<SelectableShape> {

    public ZoomMode(Mode mode) {
        super();
    }

    @Override
    public void addShape(SelectableShape s) {
    }
}
