/*
 * CropMode.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.modes;

import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.model.shapes.CropRegion;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CropMode extends AbstractMode<CropRegion> {

    private final Mode mode;

    public CropMode(Mode mode) {
        super();
        this.mode = mode;
    }

    @Override
    public void addShape(CropRegion s) {
        if (shapes.size() == 1) {
            shapes.set(0, s);
        } else {
            shapes.add(s);
        }
    }
}
