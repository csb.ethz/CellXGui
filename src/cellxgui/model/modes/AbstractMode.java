/*
 * AbstractMode.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.modes;

import cellxgui.model.shapes.SelectableShape;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public abstract class AbstractMode<ShapeT extends SelectableShape> implements Serializable {

    private static final long serialVersionUID = 1L;
    protected final List<ShapeT> shapes = new ArrayList<ShapeT>();

    public abstract void addShape(ShapeT s);

    public List<ShapeT> getShapeList() {
        return shapes;
    }

    public void deleteSelectedShapes(int[] indices) {
        for (int i = indices.length - 1; i >= 0; --i) {
            shapes.remove(indices[i]);
        }
    }

    public void setSelectedShapes(int[] indices) {
        deselectAll();
        for (int i = 0; i < indices.length; ++i) {
            shapes.get(indices[i]).setSelected(true);
        }
    }

    public void deselectAll() {
        for (ShapeT s : shapes) {
            s.setSelected(false);
        }
    }

    public boolean isEmpty() {
        return shapes.isEmpty();
    }
}
