/*
 * SeedSizeMode.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.modes;

import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.model.shapes.SeedSize;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class SeedSizeMode extends AbstractMode<SeedSize> {

    private int min, max;
    private final Mode mode;

    public SeedSizeMode(Mode mode) {
        super();
        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;
        this.mode = mode;
    }

    @Override
    public void addShape(SeedSize s) {
        shapes.add(s);
        min = Math.min(min, s.getPixelRadius());
        max = Math.max(max, s.getPixelRadius());
    }

    @Override
    public void deleteSelectedShapes(int[] indices) {
        super.deleteSelectedShapes(indices);
        min = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;
        for (SeedSize s : shapes) {
            min = Math.min(min, s.getPixelRadius());
            max = Math.max(max, s.getPixelRadius());
        }
    }

    public Integer[] getMinMaxRadius() {
        return new Integer[]{min, max};
    }
}
