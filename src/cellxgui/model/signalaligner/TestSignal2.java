package cellxgui.model.signalaligner;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class TestSignal2 implements Signal{
        
    protected double[] values;
   public TestSignal2(int maxLen, int minLen) {
        
        int l = minLen + (int)(Math.random()*(maxLen-minLen));
           
        int signalPos = (int)(Math.random()*l);
        
        
        values = new double[l];

        values[signalPos] = 1;
        
        double mean=1.0/l;
       
        for(int i=0; i<l; ++i){
            values[i] -= mean;
        }
        
    }
      
    @Override
    public double[] getNormalizedValues() {
        return values;
    }

    @Override
    public int getLength() {
        return values.length;
    }

    
}
