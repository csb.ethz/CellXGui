/*
 * MembraneSignalAligner.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
*/
package cellxgui.model.signalaligner;

import java.util.List;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembraneSignalAligner extends SignalAligner {

    protected final double thrCoverage;
    protected Double[] trimmedSignal = new Double[0];
    protected double min, max;
    protected int minPos, maxPos;
    protected int start, end;
    
    public MembraneSignalAligner(double percentageThrCoverage) {
        this.thrCoverage = percentageThrCoverage;
    }

    @Override
    public void reset() {
        super.reset();
        trimmedSignal = new Double[0];
        min = 0;
        max = 0;
        minPos = 0;
        maxPos = 0;
    }

    @Override
    public void addSignal(Signal s) {
        super.addSignal(s);
        update();
    }

    public void addAllSignals(List<? extends Signal> signals) {
        for (Signal s : signals) {
            super.addSignal(s);
        }
        if(!signals.isEmpty()){
            update();
        }
    }

    public void update() {
       // System.out.println("Update: start=" + start + " end=" + end);       
        findAutoRange();
        update(start, end);
    }

    public void updateToSubRegion(int s, int e){
        final int n = e-s;
        start += s;
        end  = start+n;
        update(start, end);
    }
    
    
    private void update(int beginIdx, int endIdx){ 
        //System.out.println(String.format("beginIdx:%d endIdx:%d  minThrCov:%d", beginIdx, endIdx, thr));
        trimmedSignal = new Double[endIdx - beginIdx];
        min = Double.MAX_VALUE;
        max = Double.MIN_VALUE;
        for (int i = 0; i < trimmedSignal.length; ++i) {
            final double val = currentConsensus[i+beginIdx];
            trimmedSignal[i] = val;
            if (val < min) {
                min = val;
                minPos = i;
            }
            if (val > max) {
                max = val;
                maxPos = i;
            }
        }
    }
    
    
    private void findAutoRange() {
        final int thr = (int) Math.ceil(thrCoverage * getMaximumSignalCoverage());
        start = 0;
        end = perPositionSignalCount.length - 1;
        for (int i = 0; i < perPositionSignalCount.length; ++i) {
            if (perPositionSignalCount[i] >= thr) {
                start = i;
                break;
            }
        }
        for (int i = perPositionSignalCount.length - 1; i >= 0; --i) {
            if (perPositionSignalCount[i] >= thr) {
                end = i+1;
                break;
            }
        }
    }

    public int getEstimateMembraneLocation() {
        if (maxPos > minPos) {
            return minPos + Math.round((maxPos - minPos) / 2);
        }
        return maxPos + Math.round((minPos - maxPos) / 2);
    }

    public int getEstimateMembraneWidth() {
        return (int) Math.abs(Math.round((maxPos - minPos) / 2.0));
    }

    public Double[] getTrimmedSignal() {
        return trimmedSignal;
    }
}
