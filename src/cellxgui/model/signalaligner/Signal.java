package cellxgui.model.signalaligner;

public interface Signal {

    double[] getNormalizedValues();

    int getLength();
}
