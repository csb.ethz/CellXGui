package cellxgui.model.signalaligner;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class XYMembraneProfile implements XYDataset {

    private final Double[] signal;
    private final SignalProperties signalProperties;
    private DatasetGroup group = new DatasetGroup();

    public XYMembraneProfile(Double[] signal) {
        this.signal = normalizeSignal(signal);
        this.signalProperties = new SignalProperties(signal);
    }

    public XYMembraneProfile() {
        this.signal = new Double[0];
        this.signalProperties = new SignalProperties(signal);
    }

    public int getMembraneLocationEstimate() {
        if (signalProperties.getMaxPos() > signalProperties.getMinPos()) {
            return signalProperties.getMinPos() + Math.round((signalProperties.getMaxPos() - signalProperties.getMinPos()) / 2);
        }
        return signalProperties.getMaxPos() + Math.round((signalProperties.getMinPos() - signalProperties.getMaxPos()) / 2);
    }

    public int getMembraneWidthEstimate() {
        return (int) Math.abs(Math.round((signalProperties.getMaxPos() - signalProperties.getMinPos()) / 2.0));
    }

    @Override
    public DomainOrder getDomainOrder() {
        return DomainOrder.ASCENDING;
    }

    @Override
    public int getItemCount(int i) {
        return signal.length;
    }

    @Override
    public Number getX(int i, int i1) {
        if (i == 0) {
            return i1;
        }
        return 0;
    }

    @Override
    public double getXValue(int i, int i1) {
        if (i == 0) {
            return i1;
        }
        return 0;
    }

    @Override
    public Number getY(int i, int i1) {
        if (i == 0) {
            return signal[i1];
        }
        return 0;
    }

    @Override
    public double getYValue(int i, int i1) {
        if (i == 0) {
            return signal[i1];
        }
        return 0;
    }

    @Override
    public int getSeriesCount() {
        return 1;
    }

    @Override
    public Comparable getSeriesKey(int i) {
        return 0;
    }

    @Override
    public int indexOf(Comparable cmprbl) {
        return 0;
    }

    @Override
    public void addChangeListener(DatasetChangeListener dl) {
    }

    @Override
    public void removeChangeListener(DatasetChangeListener dl) {
    }

    @Override
    public DatasetGroup getGroup() {
        return group;
    }

    @Override
    public void setGroup(DatasetGroup dg) {
        this.group = dg;
    }

    public static class SignalProperties {

        private final double min, max;
        private final int minPos, maxPos;

        public SignalProperties(Double[] signal) {

            double tmin, tmax;
            int tminPos = 0, tmaxPos = 0;
            tmin = Double.MAX_VALUE;
            tmax = Double.MIN_VALUE;

            for (int i = 0; i < signal.length; ++i) {
                final double val = signal[i];
                if (val < tmin) {
                    tmin = val;
                    tminPos = i;
                }
                if (val > tmax) {
                    tmax = val;
                    tmaxPos = i;
                }
            }
            this.min = tmin;
            this.max = tmax;
            this.minPos = tminPos;
            this.maxPos = tmaxPos;
        }

        public double getMax() {
            return max;
        }

        public double getMin() {
            return min;
        }

        public int getMaxPos() {
            return maxPos;
        }

        public int getMinPos() {
            return minPos;
        }
    }

    private static Double[] normalizeSignal(Double[] signal) {
        if (signal.length == 0) {
            return signal;
        }
        double mean = 0.0;
        for (int i = 0; i < signal.length; ++i) {
            mean += signal[i];
        }
        mean /= signal.length;
        for (int i = 0; i < signal.length; ++i) {
            signal[i] -= mean;
        }
        return signal;
    }
}
