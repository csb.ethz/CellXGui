/*
 * CellXBatchWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.view.LogPanel;
import java.awt.Color;
import javax.swing.JProgressBar;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXBatchWorker extends CellXCommandLineWorker {

    private final CellXController control;
    private final LogPanel logPanel;
    private final JProgressBar progressBar;
    private final BatchStreamParser batchStreamParser;

    public CellXBatchWorker(String[] cmdarray, CellXController control, LogPanel logPanel, JProgressBar progressBar) {
        super(cmdarray);
        this.control = control;
        this.logPanel = logPanel;
        this.progressBar = progressBar;
        this.batchStreamParser = new BatchStreamParser(progressBar);
    }

    @Override
    public void postCommandLineStuff() throws Exception {
        //nothing for now
    }

    @Override
    public void finallyOnSuccessInEventDispatchThread() {
        progressBar.setIndeterminate(false);
        progressBar.setValue(progressBar.getMaximum());
        progressBar.setStringPainted(true);
        progressBar.setString("done");
        //nothing for now
    }

    @Override
    public void finallyOnFailureInEventDispatchThread(Exception e) {
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(true);
        progressBar.setForeground(Color.red);
        progressBar.setString("failed");
    }

    @Override
    public void finallyOnCancelInEventDispatchThread() {
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(true);
        progressBar.setForeground(Color.red);
        progressBar.setString("canceled");
    }

    @Override
    public void handleStderr(String line) {
        logPanel.appendStderr(line);
    }

    @Override
    public void handleStdout(String line) {
        logPanel.appendStdout(line);
        batchStreamParser.parse(line);
    }
}
