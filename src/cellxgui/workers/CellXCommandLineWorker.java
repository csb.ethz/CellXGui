/*
 * CellXCommandLineWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import cellxgui.os.Kernel32;
import cellxgui.os.W32API;
import cellxgui.view.dialogs.ExecDialog;
import com.sun.jna.Pointer;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public abstract class CellXCommandLineWorker extends CommandlineWorker<Void> implements CommandlineStreamAdapter {

    private Process p;
    private long start, stop;

    public CellXCommandLineWorker(String[] cmdarray) {
        super(cmdarray);
    }

    @Override
    protected Void doInBackground() throws Exception {
        start = System.currentTimeMillis();
        p = Runtime.getRuntime().exec(cmdarray);
        startStreamReaderThreads(p, this);
        p.waitFor();
        final int exitValue = p.exitValue();
        if (exitValue != 0) {
            publish("Exit value: " + exitValue);
            throw new ExecutionException();
        }
        stop = System.currentTimeMillis();
        postCommandLineStuff();
        return null;
    }

    @Override
    protected void done() {
        try {
            get();
            firePropertyChange(ExecDialog.DONE, null, null);
            finallyOnSuccessInEventDispatchThread();
        } catch (CancellationException ce) {
            killProcess();
            firePropertyChange(ExecDialog.CANCELED, null, null);
            finallyOnCancelInEventDispatchThread();
        } catch (Exception e) {
            firePropertyChange(ExecDialog.FAILED, null, null);
            finallyOnFailureInEventDispatchThread(e);
        }
        try {
            if (p != null) {
                if (p.getInputStream() != null) {
                    p.getInputStream().close();
                }
                if (p.getOutputStream() != null) {
                    p.getOutputStream().close();
                }
                if (p.getErrorStream() != null) {
                    p.getErrorStream().close();
                }
            }
        } catch (IOException e) {
        }
        stopStreamReaderThreads();
    }

    @Override
    protected void process(List<String> chunks) {
        for (String s : chunks) {
            handleStderr(s);
        }
    }

    public abstract void postCommandLineStuff() throws Exception;

    public abstract void finallyOnSuccessInEventDispatchThread();

    public abstract void finallyOnFailureInEventDispatchThread(Exception e);

    public abstract void finallyOnCancelInEventDispatchThread();

    private void killProcess() {
        try {
            if (System.getProperty("os.name").startsWith("Windows")) {
                final Field f = p.getClass().getDeclaredField("handle");
                f.setAccessible(true);
                long handl = f.getLong(p);
                final Kernel32 kernel = Kernel32.INSTANCE;
                W32API.HANDLE handle = new W32API.HANDLE();
                handle.setPointer(Pointer.createConstant(handl));
                int pid = kernel.GetProcessId(handle);
                Runtime.getRuntime().exec(new String[]{"TASKKILL", "/PID", String.valueOf(pid), "/T", "/F"});
            } else if (System.getProperty("os.name").startsWith("Linux")) {
                final Field f = p.getClass().getDeclaredField("pid");
                f.setAccessible(true);
                int pid = (Integer) f.get(p);
                Runtime.getRuntime().exec(new String[]{"pkill", "-TERM", "-P", String.valueOf(pid)});
            } else {
                final Field f = p.getClass().getDeclaredField("pid");
                f.setAccessible(true);
                int pid = (Integer) f.get(p);
                p = Runtime.getRuntime().exec("bash");
                OutputStreamWriter ow = new OutputStreamWriter(p.getOutputStream());
                ow.write("for c in $( ps -o pid,ppid -ax  | awk \'{ if ( $2 == " + String.valueOf(pid) + " ) { print $1 } }\' )\n");
                ow.write("do\n");
                ow.write("kill -9 $c\n");
                ow.write("done\n");
                ow.write("exit\n");
                ow.flush();
                p.destroy();
            }
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(CellXCommandLineWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(CellXCommandLineWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(CellXCommandLineWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(CellXCommandLineWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CellXCommandLineWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected String getElapsedTimeString() {
        double d = (stop - start) / 1000.0;
        int mins = (int) Math.floor(d / 60);
        int secs = (int) (d - mins * 60);
        return String.format("%02dm %02ds", mins, secs);
    }
}
