/*
 * CalibrationStreamParser.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JProgressBar;

/**
 * Updates the position of a progress bar according to the CellX output
 * 
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CalibrationStreamParser implements StreamParser {

    protected final Pattern seedCountLine = Pattern.compile("^\\s*Found (\\d+) seed\\(s\\)\\s*$");
    protected final Pattern seedNumberLine = Pattern.compile("^\\s*Processing seed (\\d+)\\s*$");
    private final Pattern validatorLine = Pattern.compile("^\\s*Validating seeds ...\\s*$");
    private final Pattern resolverLine = Pattern.compile("^\\s*Resolving intersections ...\\s*$");
    private final Pattern doneLine = Pattern.compile("^\\s*done\\s*$");
    private Integer seedCount;
    protected final JProgressBar progressBar;

    CalibrationStreamParser(JProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    @Override
    public void parse(String line) {
        if (progressBar != null) {
            Matcher m = seedCountLine.matcher(line);
            if (m.matches()) {
                try {
                    seedCount = Integer.parseInt(m.group(1));
                    if (seedCount > 0) {
                        progressBar.setIndeterminate(false);
                        progressBar.setMaximum(seedCount + 3);
                    } else {
                        seedCount = null;
                    }
                } catch (NumberFormatException e) {
                }
                return;
            }

            if (seedCount != null) {
                m = seedNumberLine.matcher(line);
                if (m.matches()) {
                    try {
                        final int i = Integer.parseInt(m.group(1));
                        progressBar.setValue(i);
                    } catch (NumberFormatException e) {
                    }
                    return;
                }

                m = validatorLine.matcher(line);
                if (m.matches()) {
                    progressBar.setValue(seedCount + 1);
                    return;
                }
                m = resolverLine.matcher(line);
                if (m.matches()) {
                    progressBar.setValue(seedCount + 2);
                    return;
                }
                m = doneLine.matcher(line);
                if (m.matches()) {
                    progressBar.setValue(seedCount + 3);
                }
            }

        }
    }
}
