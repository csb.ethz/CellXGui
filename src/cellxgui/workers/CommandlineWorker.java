/*
 * CommandlineWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public abstract class CommandlineWorker<R> extends SwingWorker<R, String> {

    protected final String[] cmdarray;
    protected StreamReaderWorker stdoutWorker;
    protected StreamReaderWorker stderrWorker;

    public CommandlineWorker(String[] cmdarray) {
        this.cmdarray = cmdarray;
    }

    protected void startStreamReaderThreads(Process p, CommandlineStreamAdapter csa) throws NoSuchMethodException {
        stdoutWorker = new StreamReaderWorker(csa, CommandlineStreamAdapter.class.getMethod("handleStdout", new Class[]{String.class}));
        stderrWorker = new StreamReaderWorker(csa, CommandlineStreamAdapter.class.getMethod("handleStderr", new Class[]{String.class}));
        stdoutWorker.setStream(p.getInputStream());
        stderrWorker.setStream(p.getErrorStream());
        stderrWorker.execute();
        stdoutWorker.execute();
    }

    public void stopStreamReaderThreads() {
        if (stderrWorker != null) {
            stderrWorker.cancel(true);
        }
        if (stdoutWorker != null) {
            stdoutWorker.cancel(true);
        }
    }
}
