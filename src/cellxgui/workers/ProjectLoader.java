/*
 * ProjectLoader.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.model.BatchProcessModel;
import cellxgui.model.CellXModel;
import cellxgui.model.shapes.Coordinates;
import cellxgui.view.dialogs.WorkerDialog;
import ij.ImagePlus;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.concurrent.CancellationException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import mpicbg.ij.clahe.FastFlat;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ProjectLoader extends SwingWorker<CellXModel, String> {

    private final CellXController control;
    private final File projectFile;
    private ImagePlus ip;
    private File imgFile;
    private File relocatedImgFile;
    private File fluoImageFile;
    private File flatFieldImageFile;
    private BatchProcessModel newBatchModel;
    private boolean hasBadImage;
    private String message;

    public ProjectLoader(File f, CellXController control, File relocatedImgFile) {
        this.projectFile = f;
        this.control = control;
        this.relocatedImgFile = relocatedImgFile;
    }

    public ProjectLoader(File f, CellXController control) {
        this(f, control, null);
    }

    @Override
    protected CellXModel doInBackground() throws Exception {
        hasBadImage = false;
        final CellXModel newModel = new CellXModel();
        final FileInputStream fin = new FileInputStream(projectFile);
        final ObjectInputStream ois = new ObjectInputStream(fin);
        imgFile = (File) ois.readObject();

        final int imgWidth = ois.readInt();
        final int imgHeight = ois.readInt();

        if (relocatedImgFile != null) {
            imgFile = relocatedImgFile;
        }

        fluoImageFile = (File) ois.readObject();
        // System.out.println("Fluo file: " + fluoImageFile);      
        newModel.setFluoTestImage(fluoImageFile);

        flatFieldImageFile = (File) ois.readObject();
        //System.out.println("Flat file: " + flatFieldImageFile);
        newModel.setFlatFieldTestImage(flatFieldImageFile);

        publish("Loading parameters ...");
        final Object[] generalParameterValues = (Object[]) ois.readObject();
        final Object[] advancedParameterValues = (Object[]) ois.readObject();

        if (imgFile != null) {
            publish("Loading image ...");
            if (!imgFile.exists()) {
                hasBadImage = true;
                this.message = "Image '" + imgFile.toString() + "' no longer exists";
                throw new Exception();
            }

            ip = CellXModel.loadImageAndConvertToGrayScale(imgFile);

            if (ip.getWidth() != imgWidth || ip.getHeight() != imgHeight) {
                hasBadImage = true;
                this.message = String.format(
                        "Image dimensions do not match\n '%s' width:%d, height:%d\n expected width:%d, height:%d",
                        imgFile.toString(),
                        ip.getWidth(),
                        ip.getHeight(),
                        imgWidth,
                        imgHeight);
                throw new Exception();
            }


            final int isCLAHEEnabledIdx = CellXGuiConfiguration.getInstance().isGraphCutOnCLAHE.getLocation();
            final Boolean isCLAHE = (Boolean) advancedParameterValues[isCLAHEEnabledIdx];

            if (isCLAHE) {
                publish("Computing CLAHE ...");
                final ImagePlus copy = ip.duplicate();
                final int blockSizeIdx = CellXGuiConfiguration.getInstance().claheBlockSize.getLocation();
                final int blockRadius = (Integer) advancedParameterValues[blockSizeIdx] / 2;
                final int clipLimitIdx = CellXGuiConfiguration.getInstance().claheClipLimit.getLocation();
                final double clip = (Double) advancedParameterValues[clipLimitIdx];
                FastFlat.getInstance().run(copy, blockRadius, CellXGuiConfiguration.CLAHE_BINS, (float) clip, null, false);
                newModel.setImage(copy, imgFile);
            } else {
                newModel.setImage(ip, imgFile);
            }
        }

        publish("Loading coordinates ...");

        List<Coordinates> coords = (List<Coordinates>) ois.readObject();
        for (Coordinates c : coords) {
            newModel.addCellLength(c);
        }

        coords = (List<Coordinates>) ois.readObject();
        for (Coordinates c : coords) {
            newModel.setCrop(c);
        }

        coords = (List<Coordinates>) ois.readObject();
        for (Coordinates c : coords) {
            newModel.addMembraneProfile(c);
        }

        coords = (List<Coordinates>) ois.readObject();
        for (Coordinates c : coords) {
            newModel.addSeedSize(c);
        }

        newModel.setParameterValues(generalParameterValues);
        newModel.setAdvancedParameterValues(advancedParameterValues);

        newBatchModel = (BatchProcessModel) ois.readObject();


        ois.close();
        return newModel;
    }

    @Override
    protected void done() {
        try {
            final CellXModel m = get();
            m.setController(control);
            control.setModel(m);
            //control.setCurrentImageFileName(imgFile);
            control.setCurrentImagePlus(ip);
            //control.setFluoImage(fluoImageFile);
            //control.setFlatFieldImage(flatFieldImageFile);
            control.setBatchModel(newBatchModel);
            firePropertyChange(WorkerDialog.DONE, null, null);
            final String msg = String.format("Loaded project '%s'", projectFile.toString());
            control.propertyChange(new PropertyChangeEvent(this, CellXController.INFO_MESSAGE, null, msg));
        } catch (CancellationException ce) {
        } catch (Exception e) {
            firePropertyChange(WorkerDialog.FAILED, null, null);
            if (hasBadImage) {
                control.loadWithRelocatedImage(projectFile, message);
            } else {

                JOptionPane.showMessageDialog(control.getView(),
                        "File broken or not a CellX project",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);

                // e.printStackTrace();

            }
        }
    }

    @Override
    protected void process(List<String> chunks) {
        for (String msg : chunks) {
            firePropertyChange(WorkerDialog.STATUS_MSG, null, msg);
        }
    }

    public boolean hasBadImage() {
        return hasBadImage;
    }
}
