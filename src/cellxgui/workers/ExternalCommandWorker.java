/*
 * ExternalCommandWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public interface ExternalCommandWorker {

    void stopStreamReaderThreads();

    boolean cancel(boolean b);
}
