
package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.view.LogPanel;
import ij.ImagePlus;
import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.JProgressBar;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXCalibrationWorker extends CellXCommandLineWorker {

    private final static String LOADING_CONTROL_IMAGE = "LoadingControlImage";
    private final CellXController control;
    private final LogPanel logPanel;
    private final JProgressBar progressBar;
    /*
     *
     */
    private boolean isSegmentation;
    private File controlImageFile, fluoControlImageFile;
    private BufferedImage controlImage, fluoControlImage;

    private final CalibrationStreamParser calibrationStreamParser;
    
    public CellXCalibrationWorker(String[] cmdarray, File controlImage1, File controlImage2, boolean isSegmentation, CellXController control, LogPanel logPanel, JProgressBar progressBar) {
        super(cmdarray);
        this.control = control;
        this.logPanel = logPanel;
        this.progressBar = progressBar;
        this.controlImageFile = controlImage1;
        this.fluoControlImageFile = controlImage2;
        this.isSegmentation = isSegmentation;
        if( progressBar!=null && isSegmentation )
            this.calibrationStreamParser = new CalibrationStreamParser(progressBar);
        else
            this.calibrationStreamParser = null;
    }

    @Override
    public void postCommandLineStuff() throws Exception {
        if (controlImageFile != null) {
            publish(LOADING_CONTROL_IMAGE);
            if (!controlImageFile.exists()) {
                throw new Exception("Control image does not exist");
            }
            controlImage = new ImagePlus(controlImageFile.toString()).getBufferedImage();
        }

        if (fluoControlImageFile != null) {
            publish(LOADING_CONTROL_IMAGE);
            if (!fluoControlImageFile.exists()) {
                throw new Exception("Fluorescence control image does not exist");
            }
            fluoControlImage = new ImagePlus(fluoControlImageFile.toString()).getBufferedImage();
        }

    }

    @Override
    public void finallyOnSuccessInEventDispatchThread() {
        String execName = CellXGuiConfiguration.SegmentationName;
        if (!isSegmentation) {
            execName = CellXGuiConfiguration.HoughTransformName;
        }
        if (controlImage != null) {
            control.showControlImage(controlImage, new Point(0, 0));

            control.getView().addControlImageMenuItem(controlImageFile, execName);
        }

        if (fluoControlImage != null) {
            control.showControlImage(fluoControlImage, new Point(CellXGuiConfiguration.IMAGE_VIEWER_WIDTH + 1, 0));
            control.getView().addControlImageMenuItem(fluoControlImageFile, CellXGuiConfiguration.SegmentationFluoImageMenuName);
        }

        progressBar.setIndeterminate(false);
        progressBar.setValue(progressBar.getMaximum());
        progressBar.setStringPainted(true);
        progressBar.setString("done (" + getElapsedTimeString() +")");

    }

    @Override
    public void finallyOnFailureInEventDispatchThread(Exception e) {
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(true);
        progressBar.setForeground(Color.red);
        progressBar.setString("failed");
    }

    @Override
    public void finallyOnCancelInEventDispatchThread() {
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(true);
        progressBar.setForeground(Color.red);
        progressBar.setString("canceled");
    }

    @Override
    public void handleStderr(String line) {
        if (line.equals(LOADING_CONTROL_IMAGE)) {
            progressBar.setIndeterminate(true);
            progressBar.setStringPainted(true);
            progressBar.setString("Loading image");
        } else {
            logPanel.appendStderr(line);
        }
    }

    @Override
    public void handleStdout(String line) {
        logPanel.appendStdout(line);
        
        if( calibrationStreamParser!=null ){
            calibrationStreamParser.parse(line);
        }
  
    }
}
