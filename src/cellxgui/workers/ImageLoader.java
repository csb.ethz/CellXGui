/*
 * ImageLoader.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.model.CellXModel;
import cellxgui.view.dialogs.WorkerDialog;
import ij.ImagePlus;
import java.io.File;
import java.util.concurrent.CancellationException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ImageLoader extends SwingWorker<ImagePlus, Object> {

    protected final File imageFileName;
    protected final CellXController control;

    public ImageLoader(File imageFileName, CellXController control) {
        this.imageFileName = imageFileName;
        this.control = control;
    }

    @Override
    protected ImagePlus doInBackground() throws Exception {
        final ImagePlus ip = CellXModel.loadImageAndConvertToGrayScale(imageFileName);
        return ip;
    }

    @Override
    protected void done() {
        try {
            control.createNewModelWithImagePlus(get(), imageFileName);
            //control.setCurrentImageFileName(imageFileName);
            firePropertyChange(WorkerDialog.DONE, null, null);
        } catch (CancellationException ce) {
            //do nothing
        } catch (Exception e) {
            firePropertyChange(WorkerDialog.FAILED, null, null);
            JOptionPane.showMessageDialog(control.getView(),
                    String.format("Cannot load \n'%s'\n",
                    imageFileName.toString()),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
