/*
 * StreamReaderWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class StreamReaderWorker extends SwingWorker<Void, String> {

    private BufferedReader br;
    private boolean halt = false;
    private final Method meth;
    private final Object obj;
    private Void Void;

    public StreamReaderWorker(Object obj, Method m) {
        this.meth = m;
        this.obj = obj;
    }

    public void setStream(InputStream stream) {
        this.br = new BufferedReader(new InputStreamReader(stream));
    }

    public synchronized void stop() {
        this.halt = true;
    }

    @Override
    protected void process(List<String> chunks) {
        try {
            for (String s : chunks) {
                meth.invoke(obj, s);
            }
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "{0}", new Object[]{ex, ex});
        } catch (InvocationTargetException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "{0}", new Object[]{ex, ex});
        } catch (IllegalAccessException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "{0}", new Object[]{ex, ex});
        }
    }

    @Override
    protected Void doInBackground() throws Exception {
        while (!halt) {
            String line;
            while (null != (line = br.readLine()) && !halt) {
                publish(line);
            }
            Thread.sleep(50);
        }
        return Void;
    }
}
