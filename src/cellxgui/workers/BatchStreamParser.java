/*
 * BatchStreamParser.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JProgressBar;

/**
 * Updates the position of a progress bar according to the CellX output
 * 
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class BatchStreamParser extends CalibrationStreamParser {

    private final Pattern fileSetCountLine = Pattern.compile("^\\s*Number of file sets.*?(\\d+).*");
    private final Pattern fileSetLine = Pattern.compile("^\\s*Processing file set.*?(\\d+).*");
    protected Integer fileSetCount;
    private final static int SUB_PROGRESS_GRANULARITY = 10;
    private int currentSeedCount = 0;
    private int currentFileSet = 0;

    public BatchStreamParser(JProgressBar progressBar) {
        super(progressBar);
        fileSetCount = null;
    }

    @Override
    public void parse(String line) {
        if (progressBar != null) {
            if (fileSetCount == null) {
                Matcher m = fileSetCountLine.matcher(line);
                if (m.matches()) {
                    fileSetCount = Integer.parseInt(m.group(1));
                    if (fileSetCount > 1) {
                        progressBar.setIndeterminate(false);
                        progressBar.setMaximum((fileSetCount + 1) * SUB_PROGRESS_GRANULARITY);
                    }
                    return;
                }
            } else if (fileSetCount == 1) {
                super.parse(line);
            } else {
                Matcher m = fileSetLine.matcher(line);
                if (m.matches()) {
                    currentFileSet = Integer.parseInt(m.group(1));
                    progressBar.setValue(Math.max(1, (currentFileSet - 1) * SUB_PROGRESS_GRANULARITY));
                    return;
                }

                m = seedCountLine.matcher(line);
                if (m.matches()) {
                    currentSeedCount = Integer.parseInt(m.group(1)) + 1;
                    return;
                }

                m = seedNumberLine.matcher(line);
                if (m.matches() && currentSeedCount != 0) {
                    final int i = (currentFileSet - 1) * SUB_PROGRESS_GRANULARITY + SUB_PROGRESS_GRANULARITY * Integer.parseInt(m.group(1)) / currentSeedCount;
                    progressBar.setValue(i);
                }
            }
        }
    }
}
