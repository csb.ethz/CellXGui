package cellxgui.workers;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXGuiProperties;
import java.io.File;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXCommandBuilder {

    public static String[] getHoughTransformCmd(CellXGuiProperties properties, File tmpConfigFile, File currentImage, File tmpControlImage) {
        if (System.getProperty("os.name").startsWith("Windows")) {
            final String[] cmd = {
                properties.getCellXBatch(),
                guardFileName(tmpConfigFile.toString()),
                "-m", "files",
                "-b", guardFileName(currentImage.toString()),
                "-c1", guardFileName(tmpControlImage.toString()),
                "-cx", CellXGuiConfiguration.execCellXHoughMode};
            return cmd;
        } else {
            final String[] cmd = {
                properties.getCellXBatch(),
                guardFileName(properties.getMCR()),
                guardFileName(tmpConfigFile.toString()),
                "-m", "files",
                "-b", guardFileName(currentImage.toString()),
                "-c1", guardFileName(tmpControlImage.toString()),
                "-cx", CellXGuiConfiguration.execCellXHoughMode};
            return cmd;
        }
    }

    public static String[] getTestSegmentationCmd(CellXGuiProperties properties, File tmpConfigFile, File currentImage, File tmpControlImage, File fluoImage, File tmpFluoControlImage, File flatFieldImage) {
        String[] cmd;

        if (System.getProperty("os.name").startsWith("Windows")) {
            if (fluoImage == null) {
                cmd = new String[]{
                    properties.getCellXBatch(),
                    guardFileName(tmpConfigFile.toString()),
                    "-m", "files",
                    "-b", guardFileName(currentImage.toString()),
                    "-c1", guardFileName(tmpControlImage.toString()),
                    "-cx", CellXGuiConfiguration.execCellXSegmentationMode};

            } else {
                if (flatFieldImage == null) {
                    cmd = new String[]{
                        properties.getCellXBatch(),
                        guardFileName(tmpConfigFile.toString()),
                        "-m", "files",
                        "-b", guardFileName(currentImage.toString()),
                        "-c1", guardFileName(tmpControlImage.toString()),
                        "-cx", CellXGuiConfiguration.execCellXSegmentationMode,
                        "-f", guardFileName(fluoImage.toString()),
                        "-c2", guardFileName(tmpFluoControlImage.toString())
                    };
                } else {
                    cmd = new String[]{
                        properties.getCellXBatch(),
                        guardFileName(tmpConfigFile.toString()),
                        "-m", "files",
                        "-b", guardFileName(currentImage.toString()),
                        "-c1", guardFileName(tmpControlImage.toString()),
                        "-cx", CellXGuiConfiguration.execCellXSegmentationMode,
                        "-f", guardFileName(fluoImage.toString()),
                        "-ff", guardFileName(flatFieldImage.toString()),
                        "-c2", guardFileName(tmpFluoControlImage.toString())
                    };
                }
            }
        } else {
            if (fluoImage == null) {
                cmd = new String[]{
                    properties.getCellXBatch(),
                    guardFileName(properties.getMCR()),
                    guardFileName(tmpConfigFile.toString()),
                    "-m", "files",
                    "-b", guardFileName(currentImage.toString()),
                    "-c1", guardFileName(tmpControlImage.toString()),
                    "-cx", CellXGuiConfiguration.execCellXSegmentationMode};

            } else {
                if (flatFieldImage == null) {
                    cmd = new String[]{
                        properties.getCellXBatch(),
                        guardFileName(properties.getMCR()),
                        guardFileName(tmpConfigFile.toString()),
                        "-m", "files",
                        "-b", guardFileName(currentImage.toString()),
                        "-c1", guardFileName(tmpControlImage.toString()),
                        "-cx", CellXGuiConfiguration.execCellXSegmentationMode,
                        "-f", guardFileName(fluoImage.toString()),
                        "-c2", guardFileName(tmpFluoControlImage.toString())
                    };
                } else {
                    cmd = new String[]{
                        properties.getCellXBatch(),
                        guardFileName(properties.getMCR()),
                        guardFileName(tmpConfigFile.toString()),
                        "-m", "files",
                        "-b", guardFileName(currentImage.toString()),
                        "-c1", guardFileName(tmpControlImage.toString()),
                        "-cx", CellXGuiConfiguration.execCellXSegmentationMode,
                        "-f", guardFileName(fluoImage.toString()),
                        "-ff", guardFileName(flatFieldImage.toString()),
                        "-c2", guardFileName(tmpFluoControlImage.toString())
                    };
                }
            }
        }
        return cmd;
    }

    public static String[] getBatchCommand(CellXGuiProperties properties, File configFile, File resultsDir, File fileSeries) {
        String[] cmd;
        if (System.getProperty("os.name").startsWith("Windows")) {
            cmd = new String[]{
                properties.getCellXBatch(),
                guardFileName(configFile.toString()),
                "-m", "series",
                "-r", guardFileName(resultsDir.toString()),
                "-s", guardFileName(fileSeries.toString())
            };
        } else {
            cmd = new String[]{
                properties.getCellXBatch(),
                guardFileName(properties.getMCR()),
                guardFileName(configFile.toString()),
                "-m", "series",
                "-r", guardFileName(resultsDir.toString()),
                "-s", guardFileName(fileSeries.toString())
            };
        }
        return cmd;
    }

    public static String guardFileName(String filename) {
        return filename;
    }
}
