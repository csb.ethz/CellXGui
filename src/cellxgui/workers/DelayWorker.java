/*
 * DelayWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 */
package cellxgui.workers;

import cellxgui.view.dialogs.WorkerDialog;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class DelayWorker extends SwingWorker<Void, Void> {

    private Void Void;
    private final WorkerDialog dialog;

    public DelayWorker(WorkerDialog dialog) {
        this.dialog = dialog;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Thread.sleep(30);
        publish(Void);
        return Void;
    }

    @Override
    protected void done() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if (!dialog.isFinished()) {
                    dialog.setVisible(true);
                }
            }
        });
    }
}
