package cellxgui;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class Main {

    public static void main(String[] args) {

        if (args.length > 0 && args[0].equals("-version")) {
            System.out.println(String.format("%2d.%02d", CellXGuiConfiguration.version, CellXGuiConfiguration.release));
            return;
        }

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
        }

        java.awt.EventQueue.invokeLater(new CellXController());
    }
}
